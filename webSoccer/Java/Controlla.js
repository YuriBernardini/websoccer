$(document).ready(function(){
	
	//check real email
	function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

		
	$('#Email').keyup(function(){
		var $this = $(this);
		var emailAddress = $this.val();
        if(emailAddress!="")
        {
          if (!isValidEmailAddress(emailAddress))
          {
              $this.css({"border":"3px solid Red"});
          }
          else
          {
              $this.css({"border":"3px solid Green"});
          }
		}
        else
        {
              $this.css({"border":""});
        }
	})
	
	//Recupera
	$("#forgot-form").submit(function(event){
		event.preventDefault();
		var Email = $("#Email").val();
		// Returns successful data submission message when the entered information is stored in database.
		var dataString = 'Email='+ Email;
		if(Email=='' || !isValidEmailAddress(Email))
		{
			$("#error").html("Compilare il campo inserendo un indirizzo email valido!");
                $("#error").css("background-color","Red");
		}
		else
		{
			//AJAX code to submit form.
			$.ajax({
					type: "POST",
					url: "../System_Php/Controlla_Invia_Email.php",
					data: dataString,
					cache: false,
					success: function(result){
						if(result==1)
						{
							$("#error").html("Indirizzo email non associato ad alcun account!");
                            $("#error").css("background-color","Red");
						}
						else if(result==0)
						{
							$("#error").html("Le abbiamo inviato un email con un link di recupero a "+Email+"!");
                            $("#error").css("background-color","Green");					
						}
                                                else if(result==2)
						{
							$("#error").html("Errore nell'invio della mail riprovare");
                           $("#error").css("background-color","Red");					
						}
                                                
   
					}
											
			});
		}
                return false;
		});
	
});