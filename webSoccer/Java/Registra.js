$(document).ready(function(){
	
	function isValidEmailAddress(emailAddress){
	var espressione = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
	return espressione.test(emailAddress);
}

	var ok_email = false;
	$('#email-registra').keyup(function(){
		var $this = $(this);
		var emailAddress = $this.val();
         if(emailAddress!="")
        {
		    if (!isValidEmailAddress(emailAddress))
			{
			    $this.css({"border":"3px solid Red"});
			    ok_email = false;
			}
			else
		    {
       
			    $this.css({"border":"3px solid Green"});
			    ok_email = true;
			}
                }
                else
                {
                     $this.css({"border":""});
                }
	})
         var disp_email = false;
		 $('#email-registra').blur(function(){
			var Email = $("#email-registra").val();
			if(Email!="")
			{
				var data = 'Email='+ Email;
				$.ajax({
								type: "POST",
								url: "../System_Php/Controlla_Email.php",
								data: data,
								cache: false,
								success: function(risultato){
									if(risultato==0)
									{
										$("#Controllo_Email").html("Gia in uso!");
										$("#Controllo_Email").css("color","Red");
										disp_email = false;
									}
									else if(risultato==1)
									{
										$("#Controllo_Email").html("");
										$("#Controllo_Email").css("color","");
										disp_email = true;
									}
																

								}
														
						});
					   }
					   else
					   {
						  $("#Controllo_Email").html("");
					          $("#Controllo_Email").css("color","");
					   }
			})       
        var ok_pass= false;
        $('#pass-registra').keyup(function(){
		var Pass= $("#pass-registra").val().length;
                if(Pass!="")
                {
		    if (Pass>=8 && Pass<=20)
		    {
			 $("#Controllo_Password").html("Ok");
                         $("#Controllo_Password").css("color","Green"); 
                         ok_pass= true;
		    }
		    else
		    {
			 $("#Controllo_Password").html("Password tra 8 e 20 caratteri");
                         $("#Controllo_Password").css("color","Red"); 
                          ok_pass= false;
		    }
                }
                else
                {
                    $("#Controllo_Password").html("");
                    $("#Controllo_Password").css("color",""); 
                }
		})
        
	var disp_user = false;
	$('#user-registra').blur(function(){
		var User = $("#user-registra").val();
                if(User!="")
                {
		var data = 'Username='+ User;
		$.ajax({
						type: "POST",
						url: "../System_Php/Controlla_Username.php",
						data: data,
						cache: false,
						success: function(risultato){
							if(risultato==0)
							{
								$("#Controllo_Username").html("Non disponibile!");
								$("#Controllo_Username").css("color","Red");
								disp_user = false;
							}
							else
							{
								$("#Controllo_Username").html("Disponibile");
								$("#Controllo_Username").css("color","green");
								disp_user = true;
							}
						}
												
				});
               }
               else
               {
                $("#Controllo_Username").html("");
                $("#Controllo_Username").css("color","");  
                }
	})

	$("#reg-form").submit(function(event){
		event.preventDefault();
		var Username = $("#user-registra").val();
		var Password = $("#pass-registra").val();
		var Email = $("#email-registra").val();
		// Returns successful data submission message when the entered information is stored in database.
		var dataString = 'Username='+ Username +'&Password='+ Password +'&Email='+ Email;
		if(Username==""||Password==""||Email=="")
		{
			$("#error").html("Compilare tutti i campi!");
                        $("#error").css("background-color","Red");
		}
		else
		{

	      	if(ok_email == true)
			{
                            if(ok_pass == true)
			     {
                               if($("#user-registra").val().length<15)
			       {
				  if(disp_email == true)
				  {
                                        if(disp_user == true)
			               {
                                               

							$.ajax({
								type: "POST",
								url: "../System_Php/Registra.php",
								data: dataString,
								cache: false,
								success: function(result){
                            if(result==1)
                            {
                             $("#error").html("Abbiamo inviato una mail di registrazione a "+Email+"!");
							  $("#error").css("background-color","Green");
                              setTimeout(' window.location.href = "../System_Php/WebSoccer.php"; ',2000);
							}
                            else if(result==0)
                            {
                            	  $("#error").html("Username gia esistente!");
				                  $("#error").css("background-color","Red");
                            }
                            }
																									
						});
                                     }
                                     else
                                     {
                                             $("#error").html("Username gia esistente!");
				             $("#error").css("background-color","Red");
                                     }
			}
			else
			{
				    $("#error").html("Email gia associata ad un account!");
				    $("#error").css("background-color","Red");
			}
                     }
                    else
                    {
                       $("#error").html("Lo Username non puo essere di lunghezza superiore a 15 caratteri!");
	               $("#error").css("background-color","Red");
                    }   
                   }
                   else
                   {
                      $("#error").html("La password deve essere di lunghezza tra 8 e 20 caratteri!");
	              $("#error").css("background-color","Red");
                   }
		}
		else
		{
				$("#error").html("Inserire un indirizzo email valido!");
				$("#error").css("background-color","Red");
		}
		
		}
		});
});
	