$(document).ready(function(){
	
	var ok_pass = false;
	$("#password_1").keyup(function(){    
		var Pass= $("#password_1").val().length;
        if(Pass!="")
        {
		    if (Pass>=8 && Pass<=20)
		    {
			 $("#Controllo_Password").html("Ok");
             $("#Controllo_Password").css("color","Green"); 
             ok_pass= true;
		    }
		    else
		    {
			 $("#Controllo_Password").html("Password tra 8 e 20 caratteri");
             $("#Controllo_Password").css("color","Red"); 
             ok_pass= false;
		    }
         }
            else
            {
                $("#Controllo_Password").html("");
                $("#Controllo_Password").css("color",""); 
            }
		})
			
    
	$("#recupera-form").submit(function(event){
    	event.preventDefault();
        
          var pwd = $("#password_1").val();
          var pwd1 = $("#password_2").val(); 
          if(pwd != "" && pwd1!="")
          {    
              if(pwd==pwd1)
              {
					if(ok_pass == true)
					{
										var datastring = "pwd=" + pwd;

									   $.ajax({
										 type: "POST",
										 url: "../System_Php/Cambia_Password.php",
										 data: datastring,
										  success: function(data){		
                                                    $("#error").html("Password cambiata con successo!");
													$("#error").css("background-color","Green");
													setTimeout(' window.location.href = "../System_Php/WebSoccer.php"; ',2000);
										  }
									   });
					}
					else
					{
								$("#error").html("La password deve essere di lunghezza tra 8 e 20 caratteri!");
                                $("#error").css("background-color","Red");
					}
				}
                else
                {
                   $("#error").html("I campi password non coincidono!");
                   $("#error").css("background-color","Red");
                }
          	}
            else
            {
                $("#error").html("Compila tutti i campi!");
                $("#error").css("background-color","Red");
            }
    });
});
	