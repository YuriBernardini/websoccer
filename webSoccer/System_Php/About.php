<?php
session_start();
if(!isset($_SESSION['Utente']))
{
  header("Location:WebSoccer.php");
}
?>
<html >
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <title>WebSoccer</title>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="../css/about.css">
    <link rel="icon" href="../img/Icona.ico" type="image/png" />
</head>
<body>
<header>
	<center><a class="titolo" href="Home.php">Web Soccer</a></center>
</header>
<div class="menu">
  <ul class="topnav" id="myTopnav">
	  <li><a href="Home.php">Home</a></li>
	  <li><a href="Classifica.php">Classifica</a></li>
         <?php 
		if($_SESSION['Utente']!="Admin")
		{	
           echo "<li><a href='Profilo.php'>Profilo</a></li>";
	    }
	  ?>
	  <li><a class="active" href="About.php">Info</a></li>
	  <li><a href="Logout.php">Esci</a></li>
	  <li class="icon">
		<a href="javascript:void(0);" onclick="myFunction()">&#9776;</a>
	  </li>
  </ul>
</div>
<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
</script>
<!-- Description -->
<div class="container">
<section class="a1">
  <h1 class="insetshadow">Diventa il miglior Fanta Allenatore al mondo!</h1>
  <img src="../img/Pallone.gif" id="immagine" alt="immagine di prova">
  <p class="insetshadow">Crea la tua squadra perfetta ogni giorno.<br>Ottieni piu' punti dei tuoi avversari.<br>Scala la classifica per diventare il numero uno mondiale!</p>
</section>
<section class="i1">
</section>
<br><br>
<div class="slider">
  <div>
    <p class="teesto">
		I giocatori a tua disposizione sono moltissimi, tutti forti e soprattutto tutti italiani.
    </p>
    <p class="teesto">
		Seleziona 11 giocatori, ruolo per ruolo e preparati a battere il tuo avversario.<br><br>
    </p>
    <p class="teesto">
		Solo a fine partita potrai vedere il risultato e consultare la classifica aggiornata.
    </p>
    <p class="teesto">
		Non perdere tempo! Dopo ogni partita ne verr� un'altra, dovrai preparare una nuova squadra.
    </p>
  </div>
</div>
<!--
<article class="txt">
	<p class="deepshadow"><br>
	Crea la rosa perfetta, seleziona per ogni ruolo i campioni secondo te pi� adatti</p>
</article>-->
<br><br>
<table class="tbl">
<tr>
 <td>
	<p class="deepshadow">Scegli il miglior modulo di gioco: 433- 442- 352- 343- 532- 523</p>
 </td>
 <td>
	<div class="mod">
		  <img class="mySlides" src="../img/442.png" style="width:100%">
		  <img class="mySlides" src="../img/433.png" style="width:100%">
		  <img class="mySlides" src="../img/532.png" style="width:100%">
		  <img class="mySlides" src="../img/523.png" style="width:100%">
		  <img class="mySlides" src="../img/343.png" style="width:100%">
		  <img class="mySlides" src="../img/352.png" style="width:100%">
	</div>
 </td>
</tr>
</table>
<script>
	var myIndex = 0;
	carousel();

	function carousel() {
		var i;
		var x = document.getElementsByClassName("mySlides");
		for (i = 0; i < x.length; i++) {
		   x[i].style.display = "none";  
		}
		myIndex++;
		if (myIndex > x.length) {myIndex = 1}    
		x[myIndex-1].style.display = "block";  
		setTimeout(carousel, 2000); // Change image every 2 seconds
	}
	</script>
	<br>
	<br>

	<section class="i2"></section>
	<article class="txt1">
		<center><p class="teesto">
		Le partite si svolgono giornalmente alle ore 16:00.
		</p></center><p class="occhio">!ATTENZIONE!</p>
		<p class="teesto">Se la partita inizia e non hai creato una squadra, perderai l'incontro a tavolino.
		</p>
	</article>
	<article class="txt1">
		<p class="occhio">Sconfiggi gli avversari</p>
		<p class="teesto">Saranno attributi: 
		<br>+3pt. per la vittoria,
		<br>+1pt. per il pareggio,
		<br>+0pt. per la sconfitta.
		</p><p class="occhio">Controlla la classifica e diventa il migliori di tutti!
		</p>
	</article>
	<section class="i3"></section>
</div>
<br>
<footer>
<div class="sitename">-- WEBSOCCER� 2017� --</div>
<div class="imgn"><img src="../img/login.png"></div>
<div class="sociale">
<div class="social" onclick="window.open('https://www.facebook.com/Web-Soccer-1163412267119116','Facebook');"><img src="http://suryawahyusantoso.it.student.pens.ac.id/Tugas%20Praktikum8/images/FB.png" height="42" width="42"/></div></a>
<div class="social" onclick="window.open('https://twitter.com','Twitter');"><img src="http://3.bp.blogspot.com/-g83AOkOi5eQ/VSYV1jtoyUI/AAAAAAAACbU/D63irn0pAOU/s1600/twitter-bird.png" height="42" width="42"/></div>
<div class="social" onclick="window.open('https://accounts.google.com/Login','Twitter');"><img src="https://pilotmoon.com/popclip/extensions/icon/g.png" height="42" width="42"/></div>
</div>
<div class="autori">Powered by Bernardini Yuri & Achilli Mattia </div>
</footer>
</body>
</html>

