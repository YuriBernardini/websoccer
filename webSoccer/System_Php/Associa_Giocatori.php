<html>
<body style="background-color:black; color:white; text-align:center;">
<h1>Associazioni</h1>
<?php
include("Conn_DB.php");
function Cancella_Incontro($conn)
{
	$Data_2 = date("Y-m-d", mktime(0,0,0,date('m'), date('d') - 1, date('Y')) );
	$Query="DELETE FROM Squadra WHERE Squadra.Ultimo_Incontro='".$Data_2."'";
	$Result=mysqli_query($conn,$Query);
	$Query_1="DELETE FROM Incontro WHERE Incontro.Data='".$Data_2."'";
	$Result_1=mysqli_query($conn,$Query_1);
	if($Result && $Result_1)
	{
		return $Data_2;
	}
	else
	{
		return false;
	}
}
function Giocatori($conn,$ID_1,$ID_2)
{
	$Query="SELECT * FROM Utente WHERE Utente.ID = '".$ID_1."' OR Utente.ID = '".$ID_2."'";
	$Result = mysqli_query($conn,$Query);
	$Nomi = array();
	while($cicle=mysqli_fetch_assoc($Result))
	{
		$Nomi[] = $cicle['Username'];
	}
	return $Nomi;
}
function Partite_Domani($conn)
{
    $Data_Domani = date("Y-m-d", mktime(0,0,0,date('m'), date('d') + 1, date('Y')) );
	$Query="SELECT Id_Utente1,Id_Utente2 FROM Incontro WHERE Data = '".$Data_Domani."'";
	$Result = mysqli_query($conn,$Query); //Se non ci sono partite per oggi o domani da errore
	while($cicle=mysqli_fetch_assoc($Result))
	{
		$Giocatori[] = $cicle['Id_Utente1']."-".$cicle['Id_Utente2'];
	}
	return $Giocatori;
	
}
function Partite_Oggi($conn)
{
    $Data_Oggi = date("Y-m-d", mktime(0,0,0,date('m'),date('d') , date('Y')) );
	$Query="SELECT Id_Utente1,Id_Utente2 FROM Incontro WHERE Data = '".$Data_Oggi."'";
	$Result = mysqli_query($conn,$Query); //Se non ci sono partite per oggi o domani da errore
	while($cicle=mysqli_fetch_assoc($Result))
	{
		$Giocatori[] = $cicle['Id_Utente1']."-".$cicle['Id_Utente2'];
	}
	return $Giocatori;
	
}
function Controlla($conn)
{
	$Ora = date("H");
	if($Ora >= 16)
	{
		$domani = date("Y-m-d", mktime(0,0,0,date('m'), date('d') + 1, date('Y')) );
		$Query = "SELECT Data FROM Incontro WHERE Incontro.Data= '".$domani."'";
		$Result = mysqli_query($conn,$Query);
		if(mysqli_num_rows($Result) == 0)
		{
			$Ora = date("H");
			if($Ora==16)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else if($Ora < 16)
	{
		$Oggi = date("Y-m-d", mktime(0,0,0,date('m'), date('d'), date('Y')) );
		$Query = "SELECT Data FROM Incontro WHERE Incontro.Data= '".$Oggi."'";
		$Result = mysqli_query($conn,$Query);
		if(mysqli_num_rows($Result) > 0)
		{
			return false;
		}
	}
}
function Genera($ID)
{
	if($ID!=NULL)
	{
		$Controllo = false;
		while(!$Controllo)
		{
			$Id_1 = array_rand($ID,1);
			$Id_2 = array_rand($ID,1);
			
			$Squadra_Casa = $ID[$Id_1];
			$Squadra_Ospite = $ID[$Id_2];
			
			if($Squadra_Casa != $Squadra_Ospite)
			{
				$Risultato = $Squadra_Casa." ".$Squadra_Ospite;
				$Controllo = true;
				return $Risultato;
			}
			else
			{
				$Controllo = false;
			}
		}
	}
}
function Utenti($conn)
{
	$Query="Select ID FROM Utente";
	$Result = mysqli_query($conn,$Query);
	$Num_Row = mysqli_num_rows($Result);
	$Resto = $Num_Row % 2;
	if($Resto == 0)
	{
		return $Result;
	}
	else
	{
		$Query_1 = "SELECT * FROM Utente WHERE Utente.iD <> 30 ";
		$Result_1 = mysqli_query($conn,$Query_1);
		if($Result_1)
		{
			return $Result_1;
		}
	}
}
function Associa($conn,$ID_1,$ID_2)
{
	$Data_Domani = date("Y-m-d", mktime(0,0,0,date('m'), date('d') + 1, date('Y')) );
	$Query="INSERT INTO Incontro(Id_Utente1,Id_Utente2,Data,Esito) VALUES('$ID_1','$ID_2','$Data_Domani','')";
	$Result=mysqli_query($conn,$Query);
	if($Result)
	{
		return true;
	}
}
if(Controlla($conn))
{
	if($Data = Cancella_Incontro($conn))
	{
		echo "Cancellata squadra e incontro in data incontro $Data<br>";
	}
	else
	{
		echo "Errore nella cancellazione<br>";
	}
	if($Result=Utenti($conn))
	{
		$ID = array();
		$i=0;
		while($cicle=mysqli_fetch_assoc($Result))
		{
			$ID[$i] = $cicle['ID'];
			$i++;
		}
	}
	else if(!$Result_1=Utenti($conn))
	{
		$ID = array();
		$i=0;
		while($cicle=mysqli_fetch_assoc($Result_1))
		{
			$ID[$i] = $cicle['ID'];
			$i++;
		}
	}
		$Len_Array = count($ID);
		$x = $Len_Array/2;
		while($x != 0)
		{
			$Risultato = Genera($ID);
			$Explode = explode(" ",$Risultato);
			if(isset($Explode[0]) && isset($Explode[1]))
			{
				if(in_array($Explode[0],$ID))
				{
					unset($ID[array_search($Explode[0],$ID)]); 
					if(in_array($Explode[1],$ID))
					{
						unset($ID[array_search($Explode[1],$ID)]); 
					}
				}
				if(Associa($conn,$Explode[0],$Explode[1]))
				{
					$Nomi = Giocatori($conn,$Explode[0],$Explode[1]);
					echo "Giocatore: <b>".$Nomi[0] ."</b> associato con: <b>" .$Nomi[1]."</b><br><br>";
					$x--;
				}
			}
			
		}	
}
else
{
	$Ora = date("H");
	if($Ora > 16)
	{
		$Data_Domani = date("d-m-Y", mktime(0,0,0,date('m'), date('d') + 1, date('Y')) );
		echo "<b>Associazioni gia fatte una volta per il prossimo giorno cio�: $Data_Domani</b>";
		$Giocatori = Partite_Domani($conn);
		if($Giocatori != null)
		{
			echo "<br><br><h2>Riepilogo</h2><br><br>";
			foreach($Giocatori as $Codice)
			{
				$ID = explode("-",$Codice);
				$Nomi = Giocatori($conn,$ID[0],$ID[1]);
				echo "Giocatore: <b>".$Nomi[0] ."</b> associato con: <b>" .$Nomi[1]."</b><br><br>";
			}
		}
	}
	else if($Ora < 16)
	{
		echo "<b>Associazioni gia fatte una volta per oggi alle 16..</b>";
		$Giocatori = Partite_Oggi($conn);
		if($Giocatori != null)
		{
			echo "<br><br><h2>Riepilogo</h2><br><br>";
			foreach($Giocatori as $Codice)
			{
				$ID = explode("-",$Codice);
				$Nomi = Giocatori($conn,$ID[0],$ID[1]);
				echo "Giocatore: <b>".$Nomi[0] ."</b> associato con: <b>" .$Nomi[1]."</b><br><br>";
			}
		}
	}
}
header("location:Voti_Risultato.php");
?>
</body>
</html>