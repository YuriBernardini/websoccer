<?php

include("Conn_DB.php");

session_start();

if(!isset($_SESSION['Utente']))

{

  header("Location:WebSoccer.php");

}

?>

<html>

<head>

  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">

  <title>WebSoccer</title>

	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>

    <link rel="stylesheet" href="../css/classifica.css"> 

     <link rel="icon" href="../img/Icona.ico" type="image/png" />

</head>

<header>

	<center><a class="titolo" href="Home.php">Web Soccer</a></center>

</header>

<body>

<div class="menu">

	<ul class="topnav" id="myTopnav">

	  <li><a href="Home.php">Home</a></li>

	  <li><a class="Active" href="Classifica.php">Classifica</a></li>
	  
	  <?php 
		if($_SESSION['Utente']!="Admin")
		{
			
      echo "<li><a href='Profilo.php'>Profilo</a></li>";
	  
	  }
	  ?>

      <li><a href="About.php">Info</a></li>

	  <li><a href="Logout.php">Esci</a></li>

	  <li class="icon">

		<a href="javascript:void(0);" onclick="myFunction()">?</a>

	  </li>

	</ul>

</div>

<content>

<div class="container">

<table>

	<thead>

        <tr>

            <th>Posizione</th>

            <th>Username</th>

            <th>Nazionalita'</th>

            <th>Squadra</th>

			<th>Punti</th>

            <th>Vittorie</th>

            <th>Pareggi</th>

	    <th>Sconfitte</th>

        </tr>

    </thead>

    <tbody>

    <?php 
	function Utente_Informazione($conn)
	{
		$Query="Select * FROM Utente inner join Informazione on Informazione.Id_Utente=Utente.ID order by Informazione.punteggio DESC";
		$Result_Informazione=mysqli_query($conn,$Query);
		return $Result_Informazione;
	}
	$Result_Informazione=Utente_Informazione($conn);
	$i=1;
		if($Num_Rows=mysqli_num_rows($Result_Informazione) > 0)
		{
			while($cicle=mysqli_fetch_assoc($Result_Informazione)){
				$j=$i;
				if($cicle['Username']!=$_SESSION['Utente'])
				{
					if($cicle['Username']!="Admin")
					{
						echo "
						<div id='$i' class='modal'>
							<div class='modal-content'>
							 <div class='modal-header'>
								 <span class='close'>x</span>
								 <h2>".$cicle['Username']."</h2>					
							 </div>
							 <div class='modal-body'>
								<div class='infor'>
								<div class='sx'><i>Nazione:</i> ".$cicle['Nazione']."</div>
								<div class='dx'><i>Squadra:</i> ".$cicle['Team']."</div>
								</div>
								<div class='avatar'><img src=".$cicle['Immagine']."></div>
								<div class='cc'>".$i."&deg;</div>
							 </div>
							 <div class='modal-footer'>
							 </div>
							</div>
						</div>";
			
						echo "
				
						<tr>

							<td name='Posizione'>".$i."</td>
				 
							<td onclick='Modal($i,$j)'>".$cicle['Username']."</td>

							<td>".$cicle['Nazione']."</td>

							<td>".$cicle['Team']."</td>

							<td>".$cicle['Punteggio']."</td>

							<td>".$cicle['Vittorie']."</td>

							<td>".$cicle['Pareggi']."</td>

							<td>".$cicle['Sconfitte']."</td>

						</tr>";
					}
				}
				else
				{
					if($cicle['Username']!="Admin")
					{
						echo "
					
							<tr>

								<td name='Posizione'>".$i."</td>
					 
								<td onclick=' Redirect() '>".$cicle['Username']."</td>

								<td>".$cicle['Nazione']."</td>

								<td>".$cicle['Team']."</td>

								<td>".$cicle['Punteggio']."</td>

								<td>".$cicle['Vittorie']."</td>

								<td>".$cicle['Pareggi']."</td>

								<td>".$cicle['Sconfitte']."</td>

							</tr>";
					}
				}
			   if(!isset($Posizione))
				{
					if($cicle['Username']==$_SESSION['Utente'])
					{
						$Posizione=$i;
						$i++;
					}
					else
					{
						$i++;
					}
				}
				else
				{
					$i++;
				}
					
			 }
			function Stampa_Id($conn)
			{
				$Query_ID="Select ID FROM Utente WHERE Username='".$_SESSION['Utente']."'";
				$Result=mysqli_query($conn,$Query_ID);
				if($Result)
				{
					return $Result;
				}
				else
				{
					return false;
				}
			}
			function Stampa_Informazioni($conn,$Id)
			{
				$Query="Select * FROM Informazione WHERE Id_Utente=$Id";
				$Result_1=mysqli_query($conn,$Query);
				if($Result_1)
				{
					return $Result_1;
				}
				else
				{
					return false;
				}
			}
			function Row($Result)
			{
				$Row=mysqli_fetch_assoc($Result);
				return $Row;
			}
			function Row_1($Result_1)
			{
				$Row_1=mysqli_fetch_assoc($Result_1);
				return $Row_1;
			}
			$Result=Stampa_Id($conn);
			if($Result)
			{
				$Row=Row($Result);
				$Id=$Row['ID'];
				//Prendo le informazioni relative all'utente
				$Result_1=Stampa_Informazioni($conn,$Id);
				$Row_1=Row_1($Result_1);
				$Squadra=$Row_1['Team'];
				$Nazione=$Row_1['Nazione'];
				$Punteggio=$Row_1['Punteggio'];
				$Vittorie=$Row_1['Vittorie'];
				$Pareggi=$Row_1['Pareggi'];
				$Sconfitte=$Row_1['Sconfitte'];
			}
		}
	if($_SESSION['Utente']!="Admin")
	{
    ?>
		
		<tr>

			<td class="miopt" colspan="8">IL TUO PUNTEGGIO</td>

		</tr>

		<tr>

			<td><?php echo $Posizione; ?></td>

            <td><?php echo $_SESSION['Utente']; ?></td>

            <td><?php echo $Nazione; ?></td>

            <td><?php echo $Squadra; ?></td>

			<td><?php echo $Punteggio; ?></td>

            <td><?php echo $Vittorie; ?></td>

            <td><?php echo $Pareggi; ?></td>

            <td><?php echo $Sconfitte; ?></td>

		</tr>
  
    </tbody>

</table>
	<?php
	}
	?>
</div>

<script>
function Modal($i,$j)
{
	var modal = document.getElementById($i);

	// Get the button that opens the modal

	// Get the <span> element that closes the modal
	var span = document.getElementById($j);


	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	if (event.target == modal) {
	modal.style.display = "none";
	}
	}
}
</script>

</content>

<?php
if($_SESSION['Utente']!="Admin")
{
?>
<footer>

 <small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>

</footer>
<?php
}
?>

<script>

function myFunction() {

    var x = document.getElementById("myTopnav");

    if (x.className === "topnav") {

        x.className += " responsive";

    } else {

        x.className = "topnav";

    }

}

function Redirect(){
	 setTimeout(' window.location.href = "Profilo.php"; ',0);
	
}

</script>

</body>

</html>

<?php include("Close_DB.php"); ?>

