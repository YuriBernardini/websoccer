<?php
include("Conn_DB.php");
session_start();
if(!isset($_SESSION['Utente']))
{
  header("Location:WebSoccer.php");
}
if(!isset($_SESSION['Cookie']))
{
	echo "<div id=cookie>
	  <p>
		WebSoccer usa i cookies per darti una migliore esperienza nella navigazione. Continuando accetti la <a href='cookie.php' >nostra presenza</a> di cookie.
		<a id=close href=# onclick=javascript:nascondi()>X</a>
	  </p>
		</div>";
	$_SESSION['Cookie']=1;
}
$Username=$_SESSION['Utente'];
function Controlla_Squadra($c)
{
	if($c['Id_Giocatore1']!="" && $c['Id_Giocatore2']!="" && $c['Id_Giocatore3']!="" && $c['Id_Giocatore4']!="" && $c['Id_Giocatore5']!="" && $c['Id_Giocatore6']!="" && $c['Id_Giocatore7']!="" && $c['Id_Giocatore8']!="" && $c['Id_Giocatore9']!="" &&$c['Id_Giocatore10']!="" && $c['Id_Giocatore11']!="")
	{
		return true;
	}
	else
	{
		return false;
	}
}
function Trova_Id($conn,$Username)
{
	$Query="Select * FROM Utente WHERE Username='$Username'";
	$Result=mysqli_query($conn,$Query);
	if($Result)
	{
		return $Result;
	}
	else
	{
		return false;
	}
}
function Squadra($conn,$ID)
{
	$Query="Select * FROM Utente INNER JOIN Squadra on Squadra.Id_Utente=Utente.ID WHERE Utente.ID=$ID AND Squadra.Ultimo_Incontro is null";
	$Result_1=mysqli_query($conn,$Query);
	if($Result_1)
	{
		return $Result_1;
	}
	else
	{
		return false;
	}
}
function Row($Result)
{
	$r=mysqli_fetch_assoc($Result);
	return $r;
}
function Row_1($Result_1)
{
	$c=mysqli_fetch_assoc($Result_1);
	return $c;
}
$Result=Trova_Id($conn,$Username);
if($Result)
{
	$r=Row($Result);
	$ID=$r['ID'];
	$Result_1=Squadra($conn,$ID);
	$c=Row_1($Result_1);
}
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <title>WebSoccer</title>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="../css/home.css"> 
     <link rel="icon" href="../img/Icona.ico" type="image/png" />
</head>

<header>
	<center><a class="titolo" href="Home.php">Web Soccer</a></center>
</header>
<div class="menu">
	<ul class="topnav" id="myTopnav">
	  <li><a class="active" href="Home.php">Home</a></li>
	  <li><a href="Classifica.php">Classifica</a></li>
      <li><a href="Profilo.php">Profilo</a></li>
      <li><a href="About.php">Info</a></li>
	  <li><a href="Logout.php">Esci</a></li>
	  <li class="icon">
		<a href="javascript:void(0);" onclick="myFunction()">?</a>
	  </li>
	</ul>
	<input type="hidden" id="Id" value="<?php echo $ID; ?>">
</div>
<content>
<div class="container">
	<div class='cron' >
		<p id='cron'> Prossima partita :</p>
		<p id="demo"></p>
	</div>

<?php
	if(!Controlla_Squadra($c))
	{
		echo "<div class='alert'>
		<strong>Attenzione!</strong> Squadra non ancora creata.
		</div>";
		echo "<div onclick=location='Squadra.php'; class='uno'>
		<p>CREA SQUADRA</p>
		</div>";
	}
	else
	{
		echo "<br><div onclick=location='Riepilogo_Squadra.php'; class='tre'>
		<p>RIEPILOGO SQUADRA</p>
		</div>";
	}
?>
<?php
function Controlla_Incontro($conn,$ID)
{
	$Ora = date("H");
	if($Ora < 16)
	{
		$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') - 1 , date('Y')) );
		$Query="SELECT * FROM Incontro WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente1='".$ID."'";
		$Result=mysqli_query($conn,$Query);
		if(mysqli_num_rows($Result) > 0)
		{
			return true;
		}
		else
		{
			$Query="SELECT * FROM Incontro WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente2='".$ID."'";
			$Result=mysqli_query($conn,$Query);
			return true;
		}
	}
	else if($Ora >= 16)
	{
		$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') , date('Y')) );
		$Query="SELECT * FROM Incontro WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente1='".$ID."'";
		$Result=mysqli_query($conn,$Query);
		if(mysqli_num_rows($Result)>0)
		{
			return true;
		}
		else
		{
			$Query="SELECT * FROM Incontro WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente2='".$ID."'";
			$Result=mysqli_query($conn,$Query);
			return true;
		}
	}
}
if(Controlla_Incontro($conn,$ID))
{
	echo "
	<div onclick=location='Risultato.php'; class='due'>
	<p>VOTI ULTIMA PARTITA</p>
	</div>";
}
else
{
	echo "
	<div onclick=location='Risultato.php'; class='due' style='pointer-events:none;'>
	<p>VOTI ULTIMA PARTITA</p>
	</div>";
}
?>

<?php
function Giocatori($conn,$ID)
{
    $Ora = date("H");
	if($Ora >= 16)
	{
		$domani = date("Y-m-d", mktime(0,0,0,date('m'), date('d') + 1, date('Y')) );
		$Query="Select * FROM Incontro WHERE Incontro.Data='".$domani."' AND Incontro.Id_Utente1='".$ID."'";
		$Partita_Domani = mysqli_query($conn,$Query);
		if(mysqli_num_rows($Partita_Domani) > 0)
		{
			if($Partita_Domani)
			{
				return $Partita_Domani;
			}
		}
		else
		{
			$Query="Select * FROM Incontro WHERE Incontro.Data='".$domani."' AND Incontro.Id_Utente2='".$ID."'";
			$Partita_Domani = mysqli_query($conn,$Query);
			if($Partita_Domani)
			{
				return $Partita_Domani;
			}
		}
	}
	if($Ora < 16)
	{
		$Oggi = date("Y-m-d", mktime(0,0,0,date('m'), date('d'), date('Y')) );
		$Query="Select * FROM Incontro WHERE Incontro.Data='".$Oggi."' AND Incontro.Id_Utente1='".$ID."'";
		$Partita_Oggi = mysqli_query($conn,$Query);
		if(mysqli_num_rows($Partita_Oggi) > 0)
		{
			if($Partita_Oggi)
			{
				return $Partita_Oggi;
			}
		}
		else
		{
			$Query="Select * FROM Incontro WHERE Incontro.Data='".$Oggi."' AND Incontro.Id_Utente2='".$ID."'";
			$Partita_Oggi = mysqli_query($conn,$Query);
			if($Partita_Oggi)
			{
				return $Partita_Oggi;
			}
		}
	}

}
function Squadra_Avversaria($conn,$Username)
{
	$Query="SELECT * FROM Utente INNER JOIN Squadra ON Squadra.Id_Utente=Utente.ID WHERE Utente.Username='".$Username."' AND Squadra.Ultimo_Incontro is null";
	$Query_Squadra = mysqli_query($conn,$Query);
    if(mysqli_num_rows($Query_Squadra) > 0)
	{
		while($G=mysqli_fetch_assoc($Query_Squadra))
		{
			$Id_Giocatore[] = $G['Id_Giocatore1'];
			$Id_Giocatore[] = $G['Id_Giocatore2'];
			$Id_Giocatore[] = $G['Id_Giocatore3'];
			$Id_Giocatore[] = $G['Id_Giocatore4'];
			$Id_Giocatore[] = $G['Id_Giocatore5'];
			$Id_Giocatore[] = $G['Id_Giocatore6'];
			$Id_Giocatore[] = $G['Id_Giocatore7'];
			$Id_Giocatore[] = $G['Id_Giocatore8'];
			$Id_Giocatore[] = $G['Id_Giocatore9'];
			$Id_Giocatore[] = $G['Id_Giocatore10'];
			$Id_Giocatore[] = $G['Id_Giocatore11'];
		}
		$Query="Select * FROM Giocatore WHERE Giocatore.ID='".$Id_Giocatore[0]."' OR Giocatore.ID='".$Id_Giocatore[1]."'";
		$Query.="OR Giocatore.ID='".$Id_Giocatore[2]."' OR Giocatore.ID='".$Id_Giocatore[3]."' OR Giocatore.ID='".$Id_Giocatore[4]."'";
		$Query.="OR Giocatore.ID='".$Id_Giocatore[5]."' OR Giocatore.ID='".$Id_Giocatore[6]."' OR Giocatore.ID='".$Id_Giocatore[7]."'";
		$Query.="OR Giocatore.ID='".$Id_Giocatore[8]."' OR Giocatore.ID='".$Id_Giocatore[9]."' OR Giocatore.ID='".$Id_Giocatore[10]."'";
		$Result_2=mysqli_query($conn,$Query);
		$Nomi= array();
		while($cicle=mysqli_fetch_assoc($Result_2))
		{
			$Query_Ruolo = "SELECT Descrizione FROM Ruolo WHERE Ruolo.ID='".$cicle['Id_Ruolo']."'";
			$Result_3=mysqli_query($conn,$Query_Ruolo);
			$cicle1=mysqli_fetch_assoc($Result_3);
			$Nomi[] = trim($cicle['Nome'])." ".trim($cicle['Cognome'])." "." ".trim($cicle1['Descrizione']);
		}
		return $Nomi;
	}
	else
	{
		return false;
	}
}
$Partita = Giocatori($conn,$ID);//Incontro in base alla data
$G = mysqli_fetch_assoc($Partita);
function Trova_Giocatori($conn,$G)
{
	$Query="SELECT * FROM Utente WHERE Utente.ID='".$G['Id_Utente1']."'";
	$Giocatore = mysqli_query($conn,$Query);
	$Query_1="SELECT * FROM Utente WHERE Utente.ID='".$G['Id_Utente2']."'";
	$Giocatore_1 = mysqli_query($conn,$Query_1);
	$Array =  array();
	if($Giocatore && $Giocatore_1)
	{
		$Array[] = $Giocatore;
		$Array[] = $Giocatore_1;
		return $Array;
	}
	else
	{
		return false;
	}
}
if($Array = Trova_Giocatori($conn,$G))
{
	$Username = array();
	$Immagine= array();
	$i=0;
	$Query_1=$Array[0];
	$Query_2=$Array[1];
	while($cicle=mysqli_fetch_assoc($Query_1)){
		$Username1= $cicle['Username'];
		$Immagine1= $cicle['Immagine'];
	}
	while($cicle1=mysqli_fetch_assoc($Query_2)){
		$Username2= $cicle1['Username'];
		$Immagine2= $cicle1['Immagine'];
	}
}
if(isset($Username1) && isset($Username2))
{
	if($Username2 == $_SESSION['Utente'])
	{
		$Avversario = $Username1;
	}
	else
	{
		$Avversario = $Username2;
	}
}
?>
<div id='1' class='modal'>
        <div class='modal-content'>
                <div class='modal-header'>
                         <span class='close'>x</span>
                         <h2>Squadra di 
                         <?php 
							if($Username2 == $_SESSION['Utente'])
							{
									echo $Username1;
							}
							else
							{
									echo $Username2;
							}
                         ?>
                         </h2>                                        
                </div>
                <div class='modal-body'>
                <center>
                        <table class="tbll">
						<?php
							if($Array = Squadra_Avversaria($conn,$Avversario))
							{
								$Def=0;
								$Cen=0;
								$Att=0;
								foreach($Array as $Codice)
								{
									$Explode=explode(" ",$Codice);
									if(isset($Explode[4]))
									{
										$Explode[3]=$Explode[4];
									}
									if($Explode[3] == "Difensore")
									{
										$Def++;
									}
									if($Explode[3] == "Centrocampista")
									{	
										$Cen++;
									}
									if($Explode[3] == "Attaccante")
									{	
										$Att++;
									}
								}	
								echo "
									<tr>
									   <th colspan='2' class='td1'>Formazione: $Def-$Cen-$Att  </th>
									</tr>";
							}
							?>
                                <tr>
                                  <th class="td1">Giocatore</th>
                                  <th class="td1">Ruolo</th>
                                </tr>
                                <?php 
								if($Array = Squadra_Avversaria($conn,$Avversario))
								{
										foreach($Array as $Indice=>$Codice)
										{
											$Explode=explode(" ",$Codice);
											if(isset($Explode[4]))
											{
												$Explode[3]=$Explode[4];
											}
												echo "<tr>
												<td class='tr1'>".$Explode[0]." ".$Explode[1]." ".$Explode[2]."</td>
												<td class='tr1'>".$Explode[3]."</td>
												</tr>";
										}
								}
								else
								{
									for($i=0;$i<11;$i++)
									{
										echo "<tr>
										<td class='tr1'>/</td>
										<td class='tr1'>/</td>
										</tr>";
									}
								}
                                ?>
                        </table>
                </center>
                </div>
                <div class='modal-footer'>
                </div>
        </div>
</div>

<div class="match">
<p>Prossimo avversario</p>
	<table class="scontro">
		<tr>
		
			<td class="casa"  onclick="<?php if($Username1!=$_SESSION['Utente']){echo 'Modal()'; } else  {echo '';} ?>"><img src="
			<?php 
				if(isset($Immagine1))
				{
					echo $Immagine1;
				}
				else 
				{ 
					echo "../img/avatar.png";
				} 
			?>"
			/>
			</td>
			<td class="vs"><img src="../img/vs.png"/>
			</td>
			<td class="ospite" onclick="<?php if($Username2!=$_SESSION['Utente']){echo 'Modal()'; } else  {echo '';} ?>" ><img src="
			<?php 
				if(isset($Immagine2))
				{
					echo $Immagine2;
				}
				else 
				{ 
					echo "../img/avatar.png";
				} 
			?>"
			/>
			</td>
		</tr>
		<tr>
			<td class="name">
			<?php 
				if(isset($Username1))
				{
					echo $Username1;
				}
				else 
				{
					echo "Non associato ancora";
				} 
			?>
			</td>
			<td></td>
			<td class="name">
			<?php 
				if(isset($Username2))
				{
					echo $Username2;
				}
				else 
				{
					echo "Non associato ancora";
				} 
			?>
			</td>
		</tr>
	</table>
</div>
</div>
</content>
<footer>
 <small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>
</footer>
<script>
function Modal()
{
	var modal = document.getElementById(1);

	// Get the button that opens the modal

	// Get the <span> element that closes the modal
	var span = document.getElementById(1);


	modal.style.display = "block";

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	if (event.target == modal) {
	modal.style.display = "none";
	}
	}
}
</script>
<script>
		// Set the date we're counting down to
		var Mesi = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno",
				"Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];

            var now=new Date();
			var Domani=new Date();
			Domani.setHours(16);
			Domani.setMinutes(10);
			Domani.setSeconds(0);
			if(now.getHours() == 16 && now.getMinutes() < 10)
			{
				var Giorno = ""
				var Mese = new Date().getMonth();
				Mese = "";
				var countDownDate = new Date(Domani).getTime();
				Tipo = "Oggi";
				
			}
			else if(now.getHours() < 16)
			{
				var Giorno = ""
				var Mese = new Date().getMonth();
				Mese = "";
				var countDownDate = new Date(Domani).getTime();
				Tipo = "Oggi";
				
			}
			else if(now.getHours() == 16 && now.getMinutes() > 10)
			{
				Domani=Domani.setDate(Domani.getDate()+1);
				var Giorno = new Date(Domani).getDate();
				var Mese = new Date(Domani).getMonth();
				Mese = Mesi[Mese];
				var countDownDate = new Date(Domani).getTime();
				Tipo = "Domani, ";
			}
			else if(now.getHours() > 16)
			{
				Domani=Domani.setDate(Domani.getDate()+1);
				var Giorno = new Date(Domani).getDate();
				var Mese = new Date(Domani).getMonth();
				Mese = Mesi[Mese];
				var countDownDate = new Date(Domani).getTime();
				Tipo = "Domani, ";
			}
		//Quanto manca..
		var x = setInterval(function() {

				// Get todays date and time
				var now = new Date().getTime();
				
				// Find the distance between now an the count down date
				
				var distance = countDownDate - now;
				
				// Time calculations for days, hours, minutes and seconds
				//var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
				if(distance > 0)
				{
					// Output the result in an element with id="demo"
					document.getElementById("demo").innerHTML = Tipo + Giorno +" "+ Mese +"<br><br><br><br>" + hours + " h "
					+ minutes + " m " + seconds + " s";
				}
				
			}, 800);
</script>
<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function nascondi() {
		document.getElementById("cookie").style.display="none";
		}
</script>

</body>
</html>
<?php
include("Close_DB.php");
?>