<?php
	include("Conn_DB.php");
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <title>Recupera password</title>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="../css/forgot.css"> 
  <link rel="icon" href="../img/Icona.ico" type="image/png" />
</head>
<div id="error">
</div>
<body>
<form method="post" id="forgot-form">
<div class="container">
<div class="wrap">
                <div class="content">
                        <label class="title">Recupera Password</label>
                        <div class="campi">
                                <div class="parametri">
                                        <div class="group">
                                                <label for="Email" class="label">Indirizzo Email</label>
                                                <input id="Email" type="text" class="input">
                                        </div>
                                        <div class="group">
                                                <input type="submit" class="button" value="Conferma" id="submit" name="submit">
                                        </div>
                                        <div class="hr"></div>
                                        <div class="foot-lnk">
                                                <a href="WebSoccer.php">Voglio Accedere</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
</form>
<footer>
 <small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>
</footer>
<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../Java/Controlla.js"></script>
</body>
</html>
<?php
include("Close_DB.php");
?>