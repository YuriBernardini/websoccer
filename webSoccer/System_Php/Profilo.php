<?php
include("Conn_DB.php");
session_start();
if(!isset($_SESSION['Utente']))
{
  header("Location:WebSoccer.php");
}
else
{
	$Username=$_SESSION['Utente'];
	function Id_Immagine($Username,$conn)
	{
		$Query_ID="Select ID,Immagine FROM Utente WHERE Username='".$Username."'";
		$Result=mysqli_query($conn,$Query_ID);
		if($Result)
		{
			return $Result;
		}
		else
		{
			return false;
		}
	}
	function Informazioni_Profilo($Id,$conn)
	{
		$Query="Select * FROM Informazione WHERE Id_Utente=$Id";
		$Result_1=mysqli_query($conn,$Query);
		if($Result_1)
		{
			return $Result_1;
		}
		else
		{
			return false;
		}
	}
	function Row($Result)
	{
		$Row=mysqli_fetch_assoc($Result);
		return $Row;
	}
	function Row_1($Result_1)
	{
		$Row_1=mysqli_fetch_assoc($Result_1);
		return $Row_1;
	}
	$Result=Id_Immagine($Username,$conn);
	if($Result)
	{
		$Row=Row($Result);
		$Id=$Row['ID'];
        $Immagine=$Row['Immagine'];
		$_SESSION['Id_Utente']=$Id;
		$Result_1=Informazioni_Profilo($Id,$conn);
		$Row_1=Row_1($Result_1);
		$Squadra=$Row_1['Team'];
		$Nazione=$Row_1['Nazione'];
		$Punteggio=$Row_1['Punteggio'];
		$Vittorie=$Row_1['Vittorie'];
		$Pareggi=$Row_1['Pareggi'];
		$Sconfitte=$Row_1['Sconfitte'];
	}
}
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <title>WebSoccer</title>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="../css/profilo.css"> 
     <link rel="icon" href="../img/Icona.ico" type="image/png" />
</head>
<header>
	<center><a class="titolo" href="Home.php">Web Soccer</a></center>
</header>
<body>
<div class="menu">
	<ul class="topnav" id="myTopnav">
	  <li><a href="Home.php">Home</a></li>
	  <li><a href="Classifica.php">Classifica</a></li>
      <li><a class="Active" href="Profilo.php">Profilo</a></li>
      <li><a href="About.php">Info</a></li>
	  <li><a href="Logout.php">Esci</a></li>
	  <li class="icon">
		<a href="javascript:void(0);" onclick="myFunction()">?</a>
	  </li>
	</ul>
</div>
<content>
<div class="container">
    <img src="<?php echo $Immagine; ?>" id="Img">
	<div id="msg"></div>
	<img src="../img/1.gif" id="Load" style="display: none; width:30px; height:30px;" />
	<form id="form" action="Salva_Immagine.php" method="post" enctype="multipart/form-data">
			<br>
		<br>
		<span id="select-wrapper">
			<input type="file" name="image" id="Avatar" accept="image/*" />
		</span><br>
		<p class="text">Cambia Immagine</p>
		<br>
		<br>
		<input type='submit' id="Caricaimg" value="Carica img" 	style="display: none;"/>
			<br>
	</form>
	<br>
	<h1 class="elegant"><?php echo $_SESSION['Utente']; ?></h1>
	<div class="dati">
	<br>
	<table class="score">
	  <tr>
		<td>Punti</td>
		<td><?php echo $Punteggio; ?> </td>
	  </tr>
	  <tr>
		<td>Vittorie</td>
		<td><?php echo $Vittorie; ?> </td>
	  </tr>
	  <tr>
		<td>Pareggi</td>
		<td><?php echo $Pareggi; ?> </td>
	  </tr>
	  <tr>
		<td>Sconfitte</td>
		<td><?php echo $Sconfitte; ?> </td>
	  </tr>
	</table>
	</div>
	<div class="home">
	<table>
	  <tr>
		<th class="dx">Paese</th>
		<th class="sx">Nome Squadra</th>
	  </tr>
	  <tr>
		<td class="dx"><div class="paesi">
		<select id="Nazioni">
		<option value=" " selected> <?php if($Nazione!=" "){echo $Nazione;}else{echo "";}?></option>
		<option value="AF">Afghanistan</option>
		<option value="AL">Albania</option>
		<option value="DZ">Algeria</option>
		<option value="AS">American Samoa</option>
		<option value="AD">Andorra</option>
		<option value="AO">Angola</option>
		<option value="AI">Anguilla</option>
		<option value="AQ">Antarctica</option>
		<option value="AG">Antigua and Barbuda</option>
		<option value="AR">Argentina</option>
		<option value="AM">Armenia</option>
		<option value="AW">Aruba</option>
		<option value="AU">Australia</option>
		<option value="AT">Austria</option>
		<option value="AZ">Azerbaijan</option>
		<option value="BS">Bahamas</option>
		<option value="BH">Bahrain</option>
		<option value="BD">Bangladesh</option>
		<option value="BB">Barbados</option>
		<option value="BY">Belarus</option>
		<option value="BE">Belgium</option>
		<option value="BZ">Belize</option>
		<option value="BJ">Benin</option>
		<option value="BM">Bermuda</option>
		<option value="BT">Bhutan</option>
		<option value="BO">Bolivia</option>
		<option value="BA">Bosnia and Herzegowina</option>
		<option value="BW">Botswana</option>
		<option value="BV">Bouvet Island</option>
		<option value="BR">Brazil</option>
		<option value="IO">British Indian Ocean Territory</option>
		<option value="BN">Brunei Darussalam</option>
		<option value="BG">Bulgaria</option>
		<option value="BF">Burkina Faso</option>
		<option value="BI">Burundi</option>
		<option value="KH">Cambodia</option>
		<option value="CM">Cameroon</option>
		<option value="CA">Canada</option>
		<option value="CV">Cape Verde</option>
		<option value="KY">Cayman Islands</option>
		<option value="CF">Central African Republic</option>
		<option value="TD">Chad</option>
		<option value="CL">Chile</option>
		<option value="CN">China</option>
		<option value="CX">Christmas Island</option>
		<option value="CC">Cocos (Keeling) Islands</option>
		<option value="CO">Colombia</option>
		<option value="KM">Comoros</option>
		<option value="CG">Congo</option>
		<option value="CD">Congo, the Democratic Republic of the</option>
		<option value="CK">Cook Islands</option>
		<option value="CR">Costa Rica</option>
		<option value="CI">Cote d'Ivoire</option>
		<option value="HR">Croatia (Hrvatska)</option>
		<option value="CU">Cuba</option>
		<option value="CY">Cyprus</option>
		<option value="CZ">Czech Republic</option>
		<option value="DK">Denmark</option>
		<option value="DJ">Djibouti</option>
		<option value="DM">Dominica</option>
		<option value="DO">Dominican Republic</option>
		<option value="TP">East Timor</option>
		<option value="EC">Ecuador</option>
		<option value="EG">Egypt</option>
		<option value="SV">El Salvador</option>
		<option value="GQ">Equatorial Guinea</option>
		<option value="ER">Eritrea</option>
		<option value="EE">Estonia</option>
		<option value="ET">Ethiopia</option>
		<option value="FK">Falkland Islands (Malvinas)</option>
		<option value="FO">Faroe Islands</option>
		<option value="FJ">Fiji</option>
		<option value="FI">Finland</option>
		<option value="FR">France</option>
		<option value="FX">France, Metropolitan</option>
		<option value="GF">French Guiana</option>
		<option value="PF">French Polynesia</option>
		<option value="TF">French Southern Territories</option>
		<option value="GA">Gabon</option>
		<option value="GM">Gambia</option>
		<option value="GE">Georgia</option>
		<option value="DE">Germany</option>
		<option value="GH">Ghana</option>
		<option value="GI">Gibraltar</option>
		<option value="GR">Greece</option>
		<option value="GL">Greenland</option>
		<option value="GD">Grenada</option>
		<option value="GP">Guadeloupe</option>
		<option value="GU">Guam</option>
		<option value="GT">Guatemala</option>
		<option value="GN">Guinea</option>
		<option value="GW">Guinea-Bissau</option>
		<option value="GY">Guyana</option>
		<option value="HT">Haiti</option>
		<option value="HM">Heard and Mc Donald Islands</option>
		<option value="VA">Holy See (Vatican City State)</option>
		<option value="HN">Honduras</option>
		<option value="HK">Hong Kong</option>
		<option value="HU">Hungary</option>
		<option value="IS">Iceland</option>
		<option value="IN">India</option>
		<option value="ID">Indonesia</option>
		<option value="IR">Iran (Islamic Republic of)</option>
		<option value="IQ">Iraq</option>
		<option value="IE">Ireland</option>
		<option value="IL">Israel</option>
		<option value="IT">Italia</option>
		<option value="JM">Jamaica</option>
		<option value="JP">Japan</option>
		<option value="JO">Jordan</option>
		<option value="KZ">Kazakhstan</option>
		<option value="KE">Kenya</option>
		<option value="KI">Kiribati</option>
		<option value="KP">Korea, Democratic People's Republic of</option>
		<option value="KR">Korea, Republic of</option>
		<option value="KW">Kuwait</option>
		<option value="KG">Kyrgyzstan</option>
		<option value="LA">Lao People's Democratic Republic</option>
		<option value="LV">Latvia</option>
		<option value="LB">Lebanon</option>
		<option value="LS">Lesotho</option>
		<option value="LR">Liberia</option>
		<option value="LY">Libyan Arab Jamahiriya</option>
		<option value="LI">Liechtenstein</option>
		<option value="LT">Lithuania</option>
		<option value="LU">Luxembourg</option>
		<option value="MO">Macau</option>
		<option value="MK">Macedonia, The Former Yugoslav Republic of</option>
		<option value="MG">Madagascar</option>
		<option value="MW">Malawi</option>
		<option value="MY">Malaysia</option>
		<option value="MV">Maldives</option>
		<option value="ML">Mali</option>
		<option value="MT">Malta</option>
		<option value="MH">Marshall Islands</option>
		<option value="MQ">Martinique</option>
		<option value="MR">Mauritania</option>
		<option value="MU">Mauritius</option>
		<option value="YT">Mayotte</option>
		<option value="MX">Mexico</option>
		<option value="FM">Micronesia, Federated States of</option>
		<option value="MD">Moldova, Republic of</option>
		<option value="MC">Monaco</option>
		<option value="MN">Mongolia</option>
		<option value="MS">Montserrat</option>
		<option value="MA">Morocco</option>
		<option value="MZ">Mozambique</option>
		<option value="MM">Myanmar</option>
		<option value="NA">Namibia</option>
		<option value="NR">Nauru</option>
		<option value="NP">Nepal</option>
		<option value="NL">Netherlands</option>
		<option value="AN">Netherlands Antilles</option>
		<option value="NC">New Caledonia</option>
		<option value="NZ">New Zealand</option>
		<option value="NI">Nicaragua</option>
		<option value="NE">Niger</option>
		<option value="NG">Nigeria</option>
		<option value="NU">Niue</option>
		<option value="NF">Norfolk Island</option>
		<option value="MP">Northern Mariana Islands</option>
		<option value="NO">Norway</option>
		<option value="OM">Oman</option>
		<option value="PK">Pakistan</option>
		<option value="PW">Palau</option>
		<option value="PA">Panama</option>
		<option value="PG">Papua New Guinea</option>
		<option value="PY">Paraguay</option>
		<option value="PE">Peru</option>
		<option value="PH">Philippines</option>
		<option value="PN">Pitcairn</option>
		<option value="PL">Poland</option>
		<option value="PT">Portugal</option>
		<option value="PR">Puerto Rico</option>
		<option value="QA">Qatar</option>
		<option value="RE">Reunion</option>
		<option value="RO">Romania</option>
		<option value="RU">Russian Federation</option>
		<option value="RW">Rwanda</option>
		<option value="KN">Saint Kitts and Nevis</option> 
		<option value="LC">Saint LUCIA</option>
		<option value="VC">Saint Vincent and the Grenadines</option>
		<option value="WS">Samoa</option>
		<option value="SM">San Marino</option>
		<option value="ST">Sao Tome and Principe</option> 
		<option value="SA">Saudi Arabia</option>
		<option value="SN">Senegal</option>
		<option value="SC">Seychelles</option>
		<option value="SL">Sierra Leone</option>
		<option value="SG">Singapore</option>
		<option value="SK">Slovakia (Slovak Republic)</option>
		<option value="SI">Slovenia</option>
		<option value="SB">Solomon Islands</option>
		<option value="SO">Somalia</option>
		<option value="ZA">South Africa</option>
		<option value="GS">South Georgia and the South Sandwich Islands</option>
		<option value="ES">Spain</option>
		<option value="LK">Sri Lanka</option>
		<option value="SH">St. Helena</option>
		<option value="PM">St. Pierre and Miquelon</option>
		<option value="SD">Sudan</option>
		<option value="SR">Suriname</option>
		<option value="SJ">Svalbard and Jan Mayen Islands</option>
		<option value="SZ">Swaziland</option>
		<option value="SE">Sweden</option>
		<option value="CH">Switzerland</option>
		<option value="SY">Syrian Arab Republic</option>
		<option value="TW">Taiwan, Province of China</option>
		<option value="TJ">Tajikistan</option>
		<option value="TZ">Tanzania, United Republic of</option>
		<option value="TH">Thailand</option>
		<option value="TG">Togo</option>
		<option value="TK">Tokelau</option>
		<option value="TO">Tonga</option>
		<option value="TT">Trinidad and Tobago</option>
		<option value="TN">Tunisia</option>
		<option value="TR">Turkey</option>
		<option value="TM">Turkmenistan</option>
		<option value="TC">Turks and Caicos Islands</option>
		<option value="TV">Tuvalu</option>
		<option value="UG">Uganda</option>
		<option value="UA">Ukraine</option>
		<option value="AE">United Arab Emirates</option>
		<option value="GB">United Kingdom</option>
		<option value="US">United States</option>
		<option value="UM">United States Minor Outlying Islands</option>
		<option value="UY">Uruguay</option>
		<option value="UZ">Uzbekistan</option>
		<option value="VU">Vanuatu</option>
		<option value="VE">Venezuela</option>
		<option value="VN">Viet Nam</option>
		<option value="VG">Virgin Islands (British)</option>
		<option value="VI">Virgin Islands (U.S.)</option>
		<option value="WF">Wallis and Futuna Islands</option>
		<option value="EH">Western Sahara</option>
		<option value="YE">Yemen</option>
		<option value="YU">Yugoslavia</option>
		<option value="ZM">Zambia</option>
		<option value="ZW">Zimbabwe</option>
	</select>
		</div>
		</td>
		<td class="sx"><input type="text" id="Squadra" class="squadra" value="<?php echo $Squadra;?>" name="squadra"></td>
	  </tr>
	</table>
	</div>	
	<div class="ok">
	<img src="../img/tic.png" alt="Conferma" id="Modifica">
	<img src="../img/oktik.png" alt="Tik" id="Tik" 	style="display: none;">
	<img src="../img/load.gif" alt="Load" id="Caricamento" style="display: none;">
	</div>
	<div class="modifica2">
		<img src="../img/key.png" alt="Cambia PSW" id="Change"/>
		<p>CAMBIA PASSWORD</p>
	</div>
	<div class="modifica">
		<img src="../img/esc.png" alt="Disconnetti" id="Logout"/>
		<p>DISCONNETTI</p>
	</div>
<!-- The Modal -->
	<div id="myModal" class="modal">

	  <!-- Modal content -->
	    <div class="modal-content">
			<div class="modal-header">
				<span class="close">x</span>
				<h2>CAMBIA PASSWORD</h2>
			</div>
			<div class="modal-body">
				<div style="display:none;" id="errore"></div>
				<img src="../img/1.gif" id="spinner" style="display: none; width:30px; height:30px;" />
				<label for="Old_Password" class="label">Vecchia Password</label> 
				<br><input id="Old_Password" type="password" class="input" data-type="password">
				<br><label for="New_Password" class="label">Nuova Password</label> 
				<br><input id="New_Password" type="password" class="input" data-type="password"><br><span id="Controllo_Password"></span> 
				<br><label for="New_Password1" class="label">Ripeti Password</label> 
				<br><input id="New_Password1" type="password" class="input" data-type="password">
			</div>
		    <div class="modal-footer">
				<input type="submit" class="button" value="Cambia" id="Cambia_Password" name="submit"> 
			</div>
	    </div>

	</div>
	<script>
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	var btn = document.getElementById("Change");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on the button, open the modal 
	btn.onclick = function() {
		modal.style.display = "block";
	}

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
	</script>
</div>
<!--<h1 align="center"><?php /*echo "Profilo: ".$_SESSION['Utente']; */?></h1>-->

</content>
<footer>
 <small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>
</footer>

<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
$(document).ready(function(){
	
	$("#Logout").click(function(){
    setTimeout(' window.location.href = "Logout.php"; ',0);
		});
		
    $("#Modifica").click(function(){ 	
	var Squadra= $("#Squadra").val();
	var Nazione= $('#Nazioni :selected').text();
	if(Squadra!="")
	{
		$('#Squadra').css({"border":""});
		$('#Modifica').hide("slow");
		var datastring = "Squadra=" + Squadra+ "&Nazione="+Nazione;
		$('#Caricamento').show("slow");
		
									   $.ajax({		   
											 type: "POST",
											 url: "Informazioni_Profilo.php",
											 data: datastring,
											  success: function(data){	
											  setTimeout(function(){
												  if(data==1)
												  {			
											          $('#Caricamento').hide("slow");
													  $('#Tik').show("slow");
													  setTimeout(function(){$("#Tik").hide("slow")}, 3000);
													  setTimeout(function(){$("#Modifica").show("slow")}, 3100);
												  }
											  	}, 2000);
											  }
											  
									   });
	}
	else
	{
		$('#Squadra').css({"border":"3px solid Red"});
	}
		});	

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#Img').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
	$("#Avatar").change(function(){
		$("#msg").empty();
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
			$("#msg").html("Estensione non supportata sono supportati le seguenti estensioni: jpeg,jpg,png");
			$("#msg").css("color","Red");
			$("#Caricaimg").hide("slow");
			return false;
		}
		else
		{
			 readURL(this);
			$("#Caricaimg").show("slow");
		}
	 
});
 
   $("#form").on('submit',(function(e) {
	e.preventDefault();
                         
						 
							$('#Load').show("slow");
							$("#msg").css("color","Green");	
							setTimeout(function(){$("#msg").html("Controllo l'immagine...");}, 0);
							$.ajax({
								type: "POST",   
								url: "Salva_Immagine.php",      
								data:  new FormData(this),           
								contentType: false,       
								cache: false,             
								processData:false, 
								success: function(data)
								{		
									setTimeout(function(){$("#msg").show("slow");
										$('#Load').hide("slow");
											if(data==1)
											{											
												$("#msg").html("Immagine cambiata con successo");
												$("#msg").css("color","Green");	
												$("#Caricaimg").hide("slow");
												$("#form")[0].reset();	
											}
											else if(data==0)
											{	
												$("#msg").html("Estensione non supportata sono supportati le seguenti estensioni: jpeg,jpg,png");
												$("#msg").css("color","Red");	
												alert("Estensioni non supportate");
											}
											else if(data==2)
											{
												$("#msg").html("Dimensione dell'immagine troppo grande, nota massimo 4 MB");
												$("#msg").css("color","Red");	
											}
										
									}, 2000);
								}
							});						
	}));


   var ok_pass = false;
	$("#New_Password").keyup(function(){    
		var Pass= $("#New_Password").val().length;
        if(Pass!="")
        {
		    if (Pass>=8 && Pass<=20)
		    {
			 $("#Controllo_Password").html("Ok");
             $("#Controllo_Password").css("color","Green"); 
             ok_pass= true;
		    }
		    else
		    {
			 $("#Controllo_Password").html("Password tra 8 e 20 caratteri");
             $("#Controllo_Password").css("color","Red"); 
             ok_pass= false;
		    }
         }
            else
            {
                $("#Controllo_Password").html("");
                $("#Controllo_Password").css("color",""); 
            }
		})
		
		$("#New_Password1").keyup(function(){    
		var Pass= $("#New_Password").val();
		var Pass_1= $("#New_Password1").val();
        if(Pass_1!="")
        {
		    if(Pass==Pass_1)
			{
				$('#New_Password1').css({"border":"3px solid Green"});
			}
			else
			{
				$('#New_Password1').css({"border":""});
			}
		}
		else
		{
			$('#New_Password1').css({"border":""});
		}
		})
			
	 $("#Cambia_Password").click(function() {
		$('#errore').show("slow");
       	var Vecchia_Pwd= $("#Old_Password").val();
       	var Nuova_pwd = $("#New_Password").val();
       	var RNuova_pwd = $("#New_Password1").val();
       	if(Vecchia_Pwd!="" && Nuova_pwd!="" && RNuova_pwd!="")
       	{
       		if(Nuova_pwd==RNuova_pwd)
            {
            	if(ok_pass==true)
            	{		
                    $('#spinner').show("slow");
                    $("#errore").css("background-color","Green");
					setTimeout(function(){$("#errore").html("Controllo dei campi...");}, 0);
					var datastring = "Vecchia_Pwd=" + Vecchia_Pwd+ "&Nuova_pwd="+Nuova_pwd;
					$.ajax({
									type: "POST",   
									url: "Cambia_Password_Utente.php",      
									data: datastring,	
									success: function(data){
									setTimeout(function(){$("#errore").show("slow");
										$('#spinner').hide("slow");
										if(data==1)
										{		
											$("#errore").html("Password cambiata con successo!");
											$("#errore").css("background-color","Green");	
											setTimeout(' window.location.href = "Profilo.php"; ',2000);									
										}
										else if(data==0)
										{		
											$("#errore").html("La vecchia password non corrisponde a quella attuale!");
											$("#errore").css("background-color","Red");	
										}
									}, 2500);
									}
						});
				}
				else
				{
					$("#errore").html("La password deve essere di lunghezza tra 8 e 20 caratteri!");
					$("#errore").css("background-color","Red");
				}
		    }
			else
			{
				   $("#errore").html("I campi password non coincidono!");
                   $("#errore").css("background-color","Red");
			}
			
        } 
        else
        {
			$("#errore").html("Compilare tutti i campi!");
			$("#errore").css("background-color","Red");
        }
	});							   
 });
</script>
</body>
</html>
<?php include("Close_DB.php"); ?>