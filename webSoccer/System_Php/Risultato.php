<?php

include("Conn_DB.php");

session_start();

if(!isset($_SESSION['Utente']))

{

  header("Location:WebSoccer.php");

}
$Username = $_SESSION['Utente'];
function ID($conn,$Username)
{
	$Query = "SELECT * FROM Utente WHERE Utente.Username='".$Username."'";
	$Result = mysqli_query($conn,$Query);
	$c = mysqli_fetch_assoc($Result);
	return $c['ID'];
}
function Trova($conn,$ID,$Data)
{
	$Query="SELECT * FROM Incontro WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente1='".$ID."'";
	$Result=mysqli_query($conn,$Query);
	if(mysqli_num_rows($Result)>0)
	{
		$a=mysqli_fetch_assoc($Result);
	}
	else
	{
		$Query="SELECT * FROM Incontro WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente2='".$ID."'";
		$Result=mysqli_query($conn,$Query);
		$a=mysqli_fetch_assoc($Result);
	}
	
	$Query_1="SELECT Username FROM Utente WHERE Utente.ID='".$a['Id_Utente1']."'";
	$Result_1=mysqli_query($conn,$Query_1);
	$b= mysqli_fetch_assoc($Result_1);
	$Array[]= $b['Username']." "." ".$a['Somma_1']." ".$a['Id_Utente1']; //Array di 0 primo giocatore con somma
	
	$Query_2="SELECT Username FROM Utente WHERE Utente.ID='".$a['Id_Utente2']."'";
	$Result_2=mysqli_query($conn,$Query_2);
	$c= mysqli_fetch_assoc($Result_2);
	$Array[]= $c['Username']." "." ".$a['Somma_2']." ".$a['Id_Utente2']; //Array di 1 secondo giocatore con somma

	return $Array;
}
function Squadra_1($conn,$ID,$Data)
{
		$Query_1 = "SELECT * FROM Squadra WHERE Squadra.Ultimo_Incontro='".$Data."' AND Squadra.Id_Utente='".$ID."'";
		$Result_1 =mysqli_query($conn,$Query_1);
		if(mysqli_num_rows($Result_1) > 0)
		{
			while($G=mysqli_fetch_assoc($Result_1))
			{
					$Id_Giocatore=array();
					
					$Id_Giocatore[] = $G['Id_Giocatore1'];
					$Id_Giocatore[] = $G['Id_Giocatore2'];
					$Id_Giocatore[] = $G['Id_Giocatore3'];
					$Id_Giocatore[] = $G['Id_Giocatore4'];
					$Id_Giocatore[] = $G['Id_Giocatore5'];
					$Id_Giocatore[] = $G['Id_Giocatore6'];
					$Id_Giocatore[] = $G['Id_Giocatore7'];
					$Id_Giocatore[] = $G['Id_Giocatore8'];
					$Id_Giocatore[] = $G['Id_Giocatore9'];
					$Id_Giocatore[] = $G['Id_Giocatore10'];
					$Id_Giocatore[] = $G['Id_Giocatore11'];
			}
			return $Id_Giocatore;
		}
}
function Squadra_2($conn,$ID_2,$Data)
{
		$Query_1 = "SELECT * FROM Squadra WHERE Squadra.Ultimo_Incontro='".$Data."' AND Squadra.Id_Utente='".$ID_2."'";
		$Result_1 =mysqli_query($conn,$Query_1);
		if(mysqli_num_rows($Result_1) > 0)
		{
			while($G=mysqli_fetch_assoc($Result_1))
			{
					$Id_Giocatore=array();
					
					$Id_Giocatore[] = $G['Id_Giocatore1'];
					$Id_Giocatore[] = $G['Id_Giocatore2'];
					$Id_Giocatore[] = $G['Id_Giocatore3'];
					$Id_Giocatore[] = $G['Id_Giocatore4'];
					$Id_Giocatore[] = $G['Id_Giocatore5'];
					$Id_Giocatore[] = $G['Id_Giocatore6'];
					$Id_Giocatore[] = $G['Id_Giocatore7'];
					$Id_Giocatore[] = $G['Id_Giocatore8'];
					$Id_Giocatore[] = $G['Id_Giocatore9'];
					$Id_Giocatore[] = $G['Id_Giocatore10'];
					$Id_Giocatore[] = $G['Id_Giocatore11'];
			}
			return $Id_Giocatore;
		}
}
function Nome_Giocatori1($conn,$Id_Giocatore)
{
	
		$Query="Select * FROM Giocatore WHERE Giocatore.ID='".$Id_Giocatore[0]."' OR Giocatore.ID='".$Id_Giocatore[1]."'";
	    $Query.="OR Giocatore.ID='".$Id_Giocatore[2]."' OR Giocatore.ID='".$Id_Giocatore[3]."' OR Giocatore.ID='".$Id_Giocatore[4]."'";
		$Query.="OR Giocatore.ID='".$Id_Giocatore[5]."' OR Giocatore.ID='".$Id_Giocatore[6]."' OR Giocatore.ID='".$Id_Giocatore[7]."'";
		$Query.="OR Giocatore.ID='".$Id_Giocatore[8]."' OR Giocatore.ID='".$Id_Giocatore[9]."' OR Giocatore.ID='".$Id_Giocatore[10]."'";
		$Result=mysqli_query($conn,$Query);
		if(mysqli_num_rows($Result)>0)
		{
			$Array = array();
			while($V=mysqli_fetch_assoc($Result))
			{
				$Array[] = trim($V['Nome'])." ".trim($V['Cognome'])." "." ".$V['Voto'];
			}
			return $Array;
		}
		else
		{
			for($i=0;$i<11;$i++)
			{
				$Array[] =  ""." ".""." "." "."0";
			}
			return $Array;
		}
}
function Nome_Giocatori2($conn,$Id_Giocatore)
{
	
		$Query="Select * FROM Giocatore WHERE Giocatore.ID='".$Id_Giocatore[0]."' OR Giocatore.ID='".$Id_Giocatore[1]."'";
	    $Query.="OR Giocatore.ID='".$Id_Giocatore[2]."' OR Giocatore.ID='".$Id_Giocatore[3]."' OR Giocatore.ID='".$Id_Giocatore[4]."'";
		$Query.="OR Giocatore.ID='".$Id_Giocatore[5]."' OR Giocatore.ID='".$Id_Giocatore[6]."' OR Giocatore.ID='".$Id_Giocatore[7]."'";
		$Query.="OR Giocatore.ID='".$Id_Giocatore[8]."' OR Giocatore.ID='".$Id_Giocatore[9]."' OR Giocatore.ID='".$Id_Giocatore[10]."'";
		$Result=mysqli_query($conn,$Query);
		if(mysqli_num_rows($Result)>0)
		{
			$Array = array();
			while($V=mysqli_fetch_assoc($Result))
			{
				$Array[] = trim($V['Nome'])." ".trim($V['Cognome'])." "." ".$V['Voto'];
			}
			return $Array;
		}
		{
			for($i=0;$i<11;$i++)
			{
				$Array[] =  ""." ".""." "." "."0";
			}
			return $Array;
		}
}
$ID = ID($conn,$Username);
$Ora = date("H");
if($Ora < 16)
{
	$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') - 1 , date('Y')) );
	$Array = Trova($conn,$ID,$Data);
	
	$Explode1= explode(" ",$Array[0]);
	
	$Utente_1 = $Explode1[0]." ".$Explode1[1];
	
	if($Explode1[2] == null)
	{
		$Explode1[2] = $Explode1[3];
		if(isset($Explode1[4]))
		{
			$Explode1[3] = $Explode1[4];
		}
	}
	$Somma_1 = $Explode1[2];
	
	$Id_Utente_1= $Explode1[3];
	
	$Explode2= explode(" ",$Array[1]);

	$Utente_2 = $Explode2[0]." ".$Explode2[1];
	
	if($Explode2[2] == null)
	{
		$Explode2[2] = $Explode2[3];
		if(isset($Explode2[4]))
		{
			$Explode2[3] = $Explode2[4];
		}
	}
	$Somma_2 = $Explode2[2];
	
	$Id_Utente_2= $Explode2[3];
	
	$Squadra1 = Squadra_1($conn,$Id_Utente_2,$Data); //Ritornano ID
	
	$Squadra2 = Squadra_2($conn,$Id_Utente_1,$Data); //Ritornano ID
	
	$Squadra = Nome_Giocatori1($conn,$Squadra1); //Ritornano Nomi
		
	$Squadra_2 = Nome_Giocatori2($conn,$Squadra2); //Ritornano Nomi
}
else if($Ora >= 16)
{
	$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') , date('Y')) );
	$Array = Trova($conn,$ID,$Data);
	
	$Explode1= explode(" ",$Array[0]);
	
	$Utente_1 = $Explode1[0]." ".$Explode1[1];
	
	if($Explode1[2] == null)
	{
		$Explode1[2] = $Explode1[3];
		if(isset($Explode1[4]))
		{
			$Explode1[3] = $Explode1[4];
		}
	}
	$Somma_1 = $Explode1[2];
	
	$Id_Utente_1= $Explode1[3];
	
	$Explode2= explode(" ",$Array[1]);

	$Utente_2 = $Explode2[0]." ".$Explode2[1];
	
	if($Explode2[2] == null)
	{
		$Explode2[2] = $Explode2[3];
		if(isset($Explode2[4]))
		{
			$Explode2[3] = $Explode2[4];
		}
	}
	
	$Somma_2 = $Explode2[2];
	
	$Id_Utente_2= $Explode2[3];
	
	$Squadra1 = Squadra_1($conn,$Id_Utente_2,$Data); //Ritornano ID
	
	$Squadra2 = Squadra_2($conn,$Id_Utente_1,$Data); //Ritornano ID
	
	$Squadra = Nome_Giocatori1($conn,$Squadra1); //Ritornano Nomi
		
	$Squadra_2 = Nome_Giocatori2($conn,$Squadra2); //Ritornano Nomi
}
?>

<html>

<head>

   <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">

  <title>WebSoccer</title>

	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>

    <link rel="stylesheet" href="../css/risultato.css"> 

     <link rel="icon" href="../img/Icona.ico" type="image/png" />

</head>
<body>

<header>

	<center><a class="titolo" href="Home.php">Web Soccer</a></center>
	



<a href="Home.php"><button class="button" style="vertical-align:middle"><span>Indietro</span></button></a>
</header><div class="border"/>
<div class="container">
<table>
		<tr>
		  <th class="rivale">Casa</th>
		  <th class="username"><?php echo $Utente_1; ?></th>
		  <th class="rivale">Trasferta</th>
		  <th class="username"><?php echo $Utente_2;?></th>
		</tr>
		<tr>
		  <th class="type">Giocatore</th>
		  <th class="type">Voto</th>
		  <th class="type1" >Giocatore</th>
		  <th class="type1">Voto</th>
		</tr>
		<?php 
		for($i=0;$i<11;$i++)
		{
			$Explode = explode(" ",$Squadra[$i]);
			$Explode_1 = explode(" ",$Squadra_2[$i]);
			if(isset($Explode[4]))
			{
				$Explode[3] = $Explode[4];
			}
			if(isset($Explode_1[4]))
			{
				$Explode_1[3] = $Explode_1[4];
			}
			echo "
			<tr>
				<td class='giocatore1'>$Explode_1[0] $Explode_1[1] $Explode_1[2]</td>
				<td class='giocatore1'>$Explode_1[3]</td>
				<td class='giocatore2'>$Explode[0] $Explode[1] $Explode[2]</td>
				<td class='giocatore2'>$Explode[3]</td>
			</tr> ";
		}?>
		<tr>
		  <td class="rivale">Totale</td>
		  <td class="type"><?php echo $Somma_1;?></td>
		  <td class="rivale">Totale</td>
		  <td class="type1"><?php echo $Somma_2;?></td>
		</tr>

</table>
</div>
<div class="border"/>

<footer>

 <small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>

</footer>




</script>

</body>

</html>

<?php include("Close_DB.php"); ?>

