<?php
include("Conn_DB.php");

session_start();

if(!isset($_SESSION['Utente']))

{

  header("Location:WebSoccer.php");

}
$Username = $_SESSION['Utente'];
function Trova_Id($conn,$Username)
{
	$Query="Select * FROM Utente WHERE Username='$Username'";
	$Result=mysqli_query($conn,$Query);
	if($Result)
	{
		return $Result;
	}
	else
	{
		return false;
	}
}
function Controlla_Squadra($c)
{
	if($c['Id_Giocatore1']!="" && $c['Id_Giocatore2']!="" && $c['Id_Giocatore3']!="" && $c['Id_Giocatore4']!="" && $c['Id_Giocatore5']!="" && $c['Id_Giocatore6']!="" && $c['Id_Giocatore7']!="" && $c['Id_Giocatore8']!="" && $c['Id_Giocatore9']!="" &&$c['Id_Giocatore10']!="" && $c['Id_Giocatore11']!="")
	{
		return true;
	}
	else
	{
		return false;
	}
}
function Squadra($conn,$ID)
{
	$Query="Select * FROM Utente INNER JOIN Squadra on Squadra.Id_Utente=Utente.ID WHERE Utente.ID=$ID AND Squadra.Ultimo_Incontro is null";
	$Result_1=mysqli_query($conn,$Query);
	if($Result_1)
	{
		return $Result_1;
	}
	else
	{
		return false;
	}
}
function Row_1($Result_1)
{
	$c=mysqli_fetch_assoc($Result_1);
	return $c;
}
$Result_2=Trova_Id($conn,$Username);
if($Result_2)
{
	$A = Row_1($Result_2);
	$ID=$A['ID'];
	$Result_1=Squadra($conn,$ID);
	$c=Row_1($Result_1);
}
if(Controlla_Squadra($c))
{
	header("Location:Home.php");
}
function Portieri($conn)
{
	$Query="SELECT * FROM Giocatore WHERE Id_Ruolo=1";
	$Portiere=mysqli_query($conn, $Query);
	return $Portiere;
}
function Difensori($conn)
{
	$Query="SELECT * FROM Giocatore WHERE Id_Ruolo=2";
	$Difensore=mysqli_query($conn, $Query);
	return $Difensore;
}
function Centrocampisti($conn)
{
	$Query="SELECT * FROM Giocatore WHERE Id_Ruolo=3";
	$Centrocampista=mysqli_query($conn, $Query);
	return $Centrocampista;
}
function Attaccanti($conn)
{
	$Query="SELECT * FROM Giocatore WHERE Id_Ruolo=4";
	$Attaccante=mysqli_query($conn, $Query);
	return $Attaccante;
}
?>
<html>

<head>

  <meta charset="UTF-8">

  <title>WebSoccer</title>

	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>

    <link rel="stylesheet" href="../css/squadra.css"> 

     <link rel="icon" href="../img/Icona.ico" type="image/png" />
	 
	 <script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
	 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

</head>
<body>

<header>
	<center><a class="titolo" href="Home.php">Web Soccer</a></center>
	<a href="Home.php"><button class="button" style="vertical-align:middle"><span>Indietro</span></button></a>	
</header>
<div class="border"/>
<div class="container">
<div class="players">
<h1>Seleziona formazione:</h1>
</div>
<div class="formazione">
	<div class="tabimg">
			<span class="type" value="4-3-3">
			<img src="../img/433.png" >
			<p class="a"  style="display: none;">4-3-3</p>
			</span>
			<span class="type" value="4-4-2">
			<img src="../img/442.png">
			<p class="a"  style="display: none;">4-4-2</p>
			</span>
			<span class="type" value="3-4-3"> 
			<img src="../img/343.png">
			<p class="a"  style="display: none;">3-4-3</p>
			</span>
			<span class="type" value="3-5-2">
			<img src="../img/352.png" >
			<p class="a"  style="display: none;">3-5-2</p>
			</span>
			<span class="type" value="5-2-3"> 
			<img src="../img/523.png" >
			<p class="a"  style="display: none;">5-2-3</p>
			</span>
			<span class="type" value="5-3-2"> 
			<img src="../img/532.png"  >
			<p class="a"  style="display: none;">5-3-2</p>
			</span>		
	</div>
	<p id="Cancel">Cancel</p>
</div>
<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$Formazione = $('.type');
	$("input[type=checkbox]").attr("disabled",true);
	$Formazione.click( function() {
		
		        	$("input[type=checkbox]").attr("disabled",false);
				$Formazione.not( $(this) ).hide('slow'); // fade out every other div
				$(this).show('slow'); // fade in the div that was clicked
				$('.a').show('slow');
				setTimeout(function(){$('#Cancel').show('slow');}, 500)
			
		var Modulo= $(this).text();
		 x = Modulo.split("-");

	});
	$('#Cancel').click( function(){
		$('#Cancel').hide('slow');
		$('.a').hide('slow');
		setTimeout(function(){$Formazione.show('slow');}, 500)
		$("input[type=checkbox]").attr('checked', false);
		$("input[type=checkbox]").attr("disabled",true);
	});
	
	$("input[type=checkbox][name=portiere]").click(function() {

    var bol = $("input[type=checkbox][name=portiere]:checked").length >=1;     
    $("input[type=checkbox][name=portiere]").not(":checked").attr("disabled",bol);
	});
	
	$("input[type=checkbox][name=difensore]").click(function() {
		var bol = $("input[type=checkbox][name=difensore]:checked").length >=x[0];     
		$("input[type=checkbox][name=difensore]").not(":checked").attr("disabled",bol);
	});
	
	$("input[type=checkbox][name=centrocampista]").click(function() {
    var bol = $("input[type=checkbox][name=centrocampista]:checked").length >=x[1];     
    $("input[type=checkbox][name=centrocampista]").not(":checked").attr("disabled",bol);
	});
	
	$("input[type=checkbox][name=attaccante]").click(function() {
    var bol = $("input[type=checkbox][name=attaccante]:checked").length >=x[2];     
    $("input[type=checkbox][name=attaccante]").not(":checked").attr("disabled",bol);
	});

	$('#Conferma').click( function(){

		if($("input[type=checkbox]:checked").length >=11)
		{
			if (confirm("Sei Sicuro?")) {
				
					   var arr = $('input:checkbox').filter(':checked').map(function () {
                            return this.id;
                        }).get();
           
					    $.ajax({
								type: "POST",   
								url: "Crea_Squadra.php",      
								data: { Array : arr },
								cache:false,	
								success: function(data){
								    if(data == 1)
								    {
								        alert('Squadra creata con successo');			
				                        setTimeout(' window.location.href = "Home.php"; ',10)	
								    }
								}
									
						});
				}
		}
		else
		{
			alert('Selezionare 11 giocatori!');
		}
	});
});

</script>
<div class="players">
<h1>Seleziona Giocatori:</h1>
</div>
<div class="posizione">
 <h3>Seleziona Portiere</h3><br>
 <center>
 <table>
	<form>
	<?php
		$i=0;
		$Portiere=Portieri($conn);
		echo"<tr>";
		while($Portieri=mysqli_fetch_assoc($Portiere))
		{
			if($i==5)
			{
				$i=0;
				echo"</tr><tr>";
			}
			echo "<td class='portier'><input type='checkbox' name='portiere' id='".$Portieri['Cognome']."' value=''/> <label for='".$Portieri['Cognome']."'>".$Portieri['Nome']." ".$Portieri['Cognome']."</label><br/></td>";
			$i++;
		}
		echo"</tr>";
		
	?>
	</form>
 </table>
 </center>
</div>
<div class="border"/>
<div class="posizione">
 <h3>Seleziona Difensori</h3><br>
<center>
<table>
	<form>
	<?php
		$i=0;
		$Difensore=Difensori($conn);
		echo"<tr>";
		while($Difensori=mysqli_fetch_assoc($Difensore))
		{
			if($i==5)
			{
				$i=0;
				echo"</tr><tr>";
			}
			echo "<td class='portier'><input type='checkbox' name='difensore' id='".$Difensori['Cognome']."' value='' /><label for='".$Difensori['Cognome']."'> ".$Difensori['Nome']." ".$Difensori['Cognome']."</label><br/></td>";
			$i++;
		}
		echo"</tr>";
	?>
	 </form>
 </table>

</center>
</div>
<div class="border"/>
<div class="posizione">
 <h3>Seleziona Centrocampisti</h3><br>
<center>
 <table>
	<?php
		$i=0;
		$Centrocampista=Centrocampisti($conn);
		echo"<tr>";
		while($Centrocampisti=mysqli_fetch_assoc($Centrocampista))
		{
			if($i==5)
			{
				$i=0;
				echo"</tr><tr>";
			}
			echo "<td class='portier'><input type='checkbox' name='centrocampista' id='".$Centrocampisti['Cognome']."' value=''/><label for='".$Centrocampisti['Cognome']."'> ".$Centrocampisti['Nome']." ".$Centrocampisti['Cognome']."</label><br/></td>";
			$i++;
		}
		echo"</tr>";
	?>
 </table>
</center>
</div>
<div class="border"/>
<div class="posizione">
 <h3>Seleziona Attaccanti</h3><br>
<center>
 <table>
	<?php
		$i=0;
		$Attaccante=Attaccanti($conn);
		echo"<tr>";
		while($Attaccanti=mysqli_fetch_assoc($Attaccante))
		{
			if($i==5)
			{
				$i=0;
				echo"</tr><tr>";
			}
			echo "<td class='portier'><input type='checkbox' name='attaccante' id='".$Attaccanti['Cognome']."' value=''/><label for='".$Attaccanti['Cognome']."'> ".$Attaccanti['Nome']." ".$Attaccanti['Cognome']."</label><br/></td>";
			$i++;
		}
		echo"</tr>";
	?>
 </table>
</center>
</div>
<div class="players" id="Conferma">
<h2>CONFERMA<h2>
</div>
</div>
<footer>
 <small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>
</footer>



</body>

</html>
<?php include("Close_DB.php"); ?>


