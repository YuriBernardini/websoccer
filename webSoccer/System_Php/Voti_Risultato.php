<?php
include("Conn_DB.php");
function Controlla($conn)
{
	$Ora = date("H");
	if($Ora == 16)
	{
		return true;
	}
}
function Inserisci_Punteggi($conn,$Id_1,$Id_2)
{
	$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') , date('Y')) );
	$Query="SELECT * FROM Incontro WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente1='".$Id_1."' AND Incontro.Id_Utente2='".$Id_2."'";
	$Result=mysqli_query($conn,$Query);
	$A = mysqli_fetch_assoc($Result);
	$Esito = $A['Esito'];
	if(is_numeric($Esito))
	{
		$Query_1="SELECT * FROM Informazione WHERE Informazione.Id_Utente='".$Id_1."' OR Informazione.Id_Utente='".$Id_2."'";
		$Result_1=mysqli_query($conn,$Query_1);
		if($Esito == $Id_1)
		{
			$Punteggio=array();
			$Vittorie=array();
			$Sconfitte=array();
			while($B=mysqli_fetch_assoc($Result_1))
			{
				if($B['Id_Utente'] == $Id_1)
				{
					$Punteggio[]=$B['Punteggio'];
					$Vittorie[]=$B['Vittorie'];
				}
				else if($B['Id_Utente'] == $Id_2)
				{
					$Sconfitte[]=$B['Sconfitte'];
				}
			}
			$Vittorie= $Vittorie[0] + 1;
			$Punteggio=$Punteggio[0] + 3;
			$Sconfitte=$Sconfitte[0] + 1;
			$Query_Informazione_1="UPDATE Informazione SET Vittorie='".$Vittorie."',Punteggio='".$Punteggio."' WHERE Informazione.Id_Utente='".$Id_1."'";
			$Query_Informazione_2="UPDATE Informazione SET Sconfitte='".$Sconfitte."' WHERE Informazione.Id_Utente='".$Id_2."'";
			$Result_2=mysqli_query($conn,$Query_Informazione_1);
			
			$Result_3=mysqli_query($conn,$Query_Informazione_2);
			
		}
		else if($Esito == $Id_2)
		{
			$Punteggio=array();
			$Vittorie=array();
			$Sconfitte=array();
			while($B=mysqli_fetch_assoc($Result_1))
			{
				if($B['Id_Utente'] == $Id_2)
				{
					$Punteggio[]=$B['Punteggio'];
					$Vittorie[]=$B['Vittorie'];
				}
				else if($B['Id_Utente'] == $Id_1)
				{
					$Sconfitte[]=$B['Sconfitte'];
				}
			}
			$Vittorie= $Vittorie[0] + 1;
			$Punteggio= $Punteggio[0] + 3;
			$Sconfitte= $Sconfitte[0] + 1;
			$Query_Informazione_1="UPDATE Informazione SET Vittorie='".$Vittorie."',Punteggio='".$Punteggio."' WHERE Informazione.Id_Utente='".$Id_2."'";
			$Query_Informazione_2="UPDATE Informazione SET Sconfitte='".$Sconfitte."' WHERE Informazione.Id_Utente='".$Id_1."'";
			$Result_2=mysqli_query($conn,$Query_Informazione_1);
			$Result_3=mysqli_query($conn,$Query_Informazione_2);
		}
	}
	else
	{
		$Query_1="SELECT * FROM Informazione WHERE Informazione.Id_Utente='".$Id_1."' OR Informazione.Id_Utente='".$Id_2."'";
		$Result_1=mysqli_query($conn,$Query_1);
		$Punteggio=array();
		$Pareggi=array();
		while($B=mysqli_fetch_assoc($Result_1))
		{
			if($B['Id_Utente'] == $Id_2)
			{
				$Punteggio[0]=$B['Punteggio'];
				$Pareggi[0]=$B['Pareggi'];
			}
			else if($B['Id_Utente'] == $Id_1)
			{
				$Punteggio[1]=$B['Punteggio'];
				$Pareggi[1]=$B['Pareggi'];
			}
		}
		$Punteggio[0] += 1; //Utente2
		$Pareggi[0] += 1; //Utente2
		$Punteggio[1] += 1; //Utente1
		$Pareggi[1] += 1; //Utente1
		$Query_Informazione_1="UPDATE Informazione SET Punteggio='".$Punteggio[0]."',Pareggi='".$Pareggi[0]."' WHERE Informazione.Id_Utente='".$Id_2."'";
		$Query_Informazione_2="UPDATE Informazione SET Punteggio='".$Punteggio[1]."',Pareggi='".$Pareggi[1]."' WHERE Informazione.Id_Utente='".$Id_1."'";
		$Result_2=mysqli_query($conn,$Query_Informazione_1);
		$Result_3=mysqli_query($conn,$Query_Informazione_2);
	}
}
function Inserisci_Ultimo_Incontro_Squadra($conn,$Id_1,$Id_2)
{
	$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') , date('Y')) );
	$Query_Aggiorna="UPDATE Squadra SET Ultimo_Incontro = '".$Data."' WHERE Squadra.Id_Utente = '".$Id_1."'";
	$Result_1=mysqli_query($conn,$Query_Aggiorna);
	$Query_Aggiorna_1="UPDATE Squadra SET Ultimo_Incontro = '".$Data."' WHERE Squadra.Id_Utente = '".$Id_2."'";
	$Result_2=mysqli_query($conn,$Query_Aggiorna_1);
}
function Assegna_Voti($conn)
{
	$Query="SELECT Giocatore.ID FROM Giocatore";
	$Result=mysqli_query($conn,$Query);
	while($cicle=mysqli_fetch_assoc($Result))
	{
		$Voto=rand(3,12);
		$ID = $cicle['ID'];
		$Query_Aggiorna="UPDATE Giocatore SET Voto = '".$Voto."' WHERE Giocatore.ID = '".$ID."'";
		$Result_1=mysqli_query($conn,$Query_Aggiorna);
	}
	if($Result_1)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function Stampa_Utenti($conn)
{
	$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') , date('Y')) );
	$Query="SELECT Id_Utente1,Id_Utente2 FROM Incontro WHERE Incontro.Data='".$Data."'";
	$Result=mysqli_query($conn,$Query);
	$Array_Utenti= array();
	while($U=mysqli_fetch_assoc($Result))
	{
		$Array_Utenti[$U['Id_Utente1']]=$U['Id_Utente2'];
	}
	return $Array_Utenti;
}
function Calcola_Vincitore($Id_1,$Id_2,$Tot,$conn)
{
	$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') , date('Y')) );
	$Explode = explode("-",$Tot);
	
	$Query_Aggiorna_Somma1="UPDATE Incontro SET Somma_1 = '".$Explode[0]."' WHERE Incontro.Data = '".$Data."' AND Incontro.Id_Utente1 = '".$Id_1."' AND Incontro.Id_Utente2 = '".$Id_2."' ";
	
	$Aggiorna1 = mysqli_query($conn,$Query_Aggiorna_Somma1); 
	
	$Query_Aggiorna_Somma2="UPDATE Incontro SET Somma_2 = '".$Explode[1]."' WHERE Incontro.Data = '".$Data."' AND Incontro.Id_Utente1 = '".$Id_1."' AND Incontro.Id_Utente2 = '".$Id_2."' ";
	
	$Aggiorna2 = mysqli_query($conn,$Query_Aggiorna_Somma2); 
	
	if($Explode[0] > $Explode[1])
	{
		$Query1="UPDATE Incontro SET Esito = '".$Id_1."' WHERE Incontro.Data = '".$Data."' AND Incontro.Id_Utente1 = '".$Id_1."' AND Incontro.Id_Utente2 = '".$Id_2."' ";

	    $Aggiorna3 = mysqli_query($conn,$Query1); 
	}
	
	if($Explode[0] < $Explode[1])
	{
		$Query2="UPDATE Incontro SET Esito = '".$Id_2."' WHERE Incontro.Data = '".$Data."' AND Incontro.Id_Utente1 = '".$Id_1."' AND Incontro.Id_Utente2 = '".$Id_2."' ";

	    $Aggiorna4 = mysqli_query($conn,$Query2); 
	}
	
	if($Explode[0] == $Explode[1])
	{
		$Query3="UPDATE Incontro SET Esito = 'X' WHERE Incontro.Data = '".$Data."' AND Incontro.Id_Utente1 = '".$Id_1."' AND Incontro.Id_Utente2 = '".$Id_2."' ";

	    $Aggiorna5 = mysqli_query($conn,$Query3); 
	}
}
function Totale_Voti($Id_1,$Id_2,$conn)
{
	$Data = date("Y-m-d", mktime(0,0,0,date('m'), date('d') , date('Y')) );
	$Query="SELECT * FROM Incontro INNER JOIN Squadra ON Squadra.Id_Utente=Incontro.Id_Utente1 WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente1='".$Id_1."'";
	$Result=mysqli_query($conn,$Query);
	if(mysqli_num_rows($Result) > 0)
	{
		$Somma_1=0;
		$Id_Giocatore=array();
		while($G=mysqli_fetch_assoc($Result))
		{
				$Id_Giocatore[0] = $G['Id_Giocatore1'];
				$Id_Giocatore[1] = $G['Id_Giocatore2'];
				$Id_Giocatore[2] = $G['Id_Giocatore3'];
				$Id_Giocatore[3] = $G['Id_Giocatore4'];
				$Id_Giocatore[4] = $G['Id_Giocatore5'];
				$Id_Giocatore[5] = $G['Id_Giocatore6'];
				$Id_Giocatore[6] = $G['Id_Giocatore7'];
				$Id_Giocatore[7] = $G['Id_Giocatore8'];
				$Id_Giocatore[8] = $G['Id_Giocatore9'];
				$Id_Giocatore[9] = $G['Id_Giocatore10'];
				$Id_Giocatore[10] = $G['Id_Giocatore11'];
		}
		
					$Query_G="Select Voto FROM Giocatore WHERE Giocatore.ID='".$Id_Giocatore[0]."' OR Giocatore.ID='".$Id_Giocatore[1]."'";
					$Query_G.="OR Giocatore.ID='".$Id_Giocatore[2]."' OR Giocatore.ID='".$Id_Giocatore[3]."' OR Giocatore.ID='".$Id_Giocatore[4]."'";
					$Query_G.="OR Giocatore.ID='".$Id_Giocatore[5]."' OR Giocatore.ID='".$Id_Giocatore[6]."' OR Giocatore.ID='".$Id_Giocatore[7]."'";
					$Query_G.="OR Giocatore.ID='".$Id_Giocatore[8]."' OR Giocatore.ID='".$Id_Giocatore[9]."' OR Giocatore.ID='".$Id_Giocatore[10]."'";
					$Result_3=mysqli_query($conn,$Query_G);
					while($V=mysqli_fetch_assoc($Result_3))
					{
						$Somma_1 = $Somma_1 + $V['Voto'];
					}
	}
	else
	{
		$Somma_1 = 0;
	}
	$Query_1="SELECT * FROM Incontro INNER JOIN Squadra ON Squadra.Id_Utente=Incontro.Id_Utente2 WHERE Incontro.Data='".$Data."' AND Incontro.Id_Utente2='".$Id_2."'";
	$Result_1=mysqli_query($conn,$Query_1);
	if(mysqli_num_rows($Result_1)>0)
	{
		$Somma_2=0;
		$Id_Giocatore_1=array();
		while($A=mysqli_fetch_assoc($Result_1))
		{
				
				$Id_Giocatore_1[0] = $A['Id_Giocatore1'];
				$Id_Giocatore_1[1] = $A['Id_Giocatore2'];
				$Id_Giocatore_1[2] = $A['Id_Giocatore3'];
				$Id_Giocatore_1[3] = $A['Id_Giocatore4'];
				$Id_Giocatore_1[4] = $A['Id_Giocatore5'];
				$Id_Giocatore_1[5] = $A['Id_Giocatore6'];
				$Id_Giocatore_1[6] = $A['Id_Giocatore7'];
				$Id_Giocatore_1[7] = $A['Id_Giocatore8'];
				$Id_Giocatore_1[8] = $A['Id_Giocatore9'];
				$Id_Giocatore_1[9] = $A['Id_Giocatore10'];
				$Id_Giocatore_1[10] = $A['Id_Giocatore11'];
		}
					$Query_A="Select Voto FROM Giocatore WHERE Giocatore.ID='".$Id_Giocatore_1[0]."' OR Giocatore.ID='".$Id_Giocatore_1[1]."'";
					$Query_A.="OR Giocatore.ID='".$Id_Giocatore_1[2]."' OR Giocatore.ID='".$Id_Giocatore_1[3]."' OR Giocatore.ID='".$Id_Giocatore_1[4]."'";
					$Query_A.="OR Giocatore.ID='".$Id_Giocatore_1[5]."' OR Giocatore.ID='".$Id_Giocatore_1[6]."' OR Giocatore.ID='".$Id_Giocatore_1[7]."'";
					$Query_A.="OR Giocatore.ID='".$Id_Giocatore_1[8]."' OR Giocatore.ID='".$Id_Giocatore_1[9]."' OR Giocatore.ID='".$Id_Giocatore_1[10]."'";
					$Result_4=mysqli_query($conn,$Query_A);
					while($M=mysqli_fetch_assoc($Result_4))
					{
						$Somma_2 = $Somma_2 + $M['Voto'];
					}
	}
	else
	{
		$Somma_2 = 0;
	}
	$Risultato_Tot = $Somma_1."-".$Somma_2;
	return $Risultato_Tot;
}
if(Controlla($conn))
{
	if(Assegna_Voti($conn))
	{
		echo "<b>Voti ai giocatori assegnati</b><br><br>";
		if($Array=Stampa_Utenti($conn))
		{
			echo "<table border=1>";
			echo "<tr><td>Id_1</td><td>Id_2</td><td>Esito</td></tr>";
			foreach($Array as $U_1=>$U_2)
			{
				$Tot = Totale_Voti($U_1,$U_2,$conn);
				echo "<tr><td>$U_1</td><td>$U_2</td><td>$Tot</td></tr>";	
				Calcola_Vincitore($U_1,$U_2,$Tot,$conn);
				Inserisci_Punteggi($conn,$U_1,$U_2);
				Inserisci_Ultimo_Incontro_Squadra($conn,$U_1,$U_2);
			}
			echo "</table><br>";
		}
	}
	else
	{
		echo "Errore";
	}
}
else
{
	echo "<b>Errore nell'ora</b><br><br>";
}
?>