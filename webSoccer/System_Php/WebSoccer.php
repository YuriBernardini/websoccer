<?php
include("Conn_DB.php");
?>
<!DOCTYPE html>
<html >
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <title>WebSoccer</title>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="../css/login.css"> 
  <link rel="icon" href="../img/Icona.ico" type="image/png" />
</head>
<!--
<header>
	<center><h1>BENVENUTO IN WebSoccer</h1></center></small>
</header>
-->
<body>
<form method="post" id="login-form">
<div id="error"></div>
<div class="container">
	<div class="wrap">
                <div class="content">
                        <label class="title">Accedi</label>
<!--                <label class="title"><a href="registrati.php">Registrati</a></label>        -->
                        <div class="campi">
                                <div class="parametri">
                                        <div class="group">
                                                <label for="user-login" class="label">Username</label>
                                                <input id="user-login" type="text" class="input">
                                        </div>
                                        <div class="group">
                                                <label for="pass-login" class="label">Password</label>
                                                <input id="pass-login" type="password" class="input" data-type="password">
                                        </div>
                                        <div class="group">
                                                <input type="submit" class="button" value="Accedi" id="submit" name="submit">
                                        </div>
                                        <div class="foot-lnk">
                                                <p>Non Sei Registrato? <a class="reg" href="WebSoccer_Register.php">Registrati</a></p>
                                        </div><div class="hr"></div>
                                        <div class="foot-lnk">
                                                <a href="Password_Dimenticata.php">Password Dimenticata?</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>



</div>
</form>
<footer>
 <small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>
</footer>
<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../Java/Login.js"></script>
</body>
</html>
<?php
include("Close_DB.php");
?>