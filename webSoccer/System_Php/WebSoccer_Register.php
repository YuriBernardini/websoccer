<?php
	include("Conn_DB.php");
?>
<!DOCTYPE html>
<html >
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <title>WebSoccer</title>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="../css/register.css"> 
  <link rel="icon" href="../img/Icona.ico" type="image/png" />
</head>
<body>
<form method="post" id="reg-form">
<div id="error"></div>
<div class="container">
	<div class="wrap">
                <div class="content">
                        <label class="title">Registrati</label>
                        <div class="campi">
                                <div class="parametri">
                                        <div class="group">
                                                <label for="user-registra" class="label">Username</label>
                                                <input id="user-registra" type="text" class="input"><span id="Controllo_Username"></span>
                                        </div>
                                        <div class="group">
                                                <label for="email-registra" class="label">Indirizzo Email</label>
                                                <input id="email-registra" type="text" class="input"><span id="Controllo_Email"></span>
                                        </div>
                                        <div class="group">
                                                <label for="pass-registra" class="label">Password</label>
                                                <input id="pass-registra" type="password" class="input" data-type="password"><span id="Controllo_Password"></span>
                                        </div>
                                        <div class="group">
                                                <input type="submit" class="button" value="Registrati" id="submit" name="submit">
                                        </div>
                                        <div class="hr"></div>
                                        <div class="foot-lnk">
                                                <a href="WebSoccer.php">Gia Registrato?</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
</form>
<footer>
<small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>
</footer>
<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../Java/Registra.js"></script>
</body>
</html>
<?php
include("Close_DB.php");
?>