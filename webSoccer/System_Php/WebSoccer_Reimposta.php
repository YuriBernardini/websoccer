<?php
	session_start();
	include("Conn_DB.php");
    if(!isset($_SESSION['Codice']))
    {
    	header("Location:WebSoccer.php");
    }
?>
<!DOCTYPE html>
<html >
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <title>WebSoccer</title>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
    <link rel="stylesheet" href="../css/reimposta.css"> 
  <link rel="icon" href="../img/Icona.ico" type="image/png" />
</head>
<body>
<form method="post" id="recupera-form">
<div id="error"></div>
<div class="container">
	<div class="wrap">
                <div class="content">
                        <label class="title">Reimposta Password</label>
                        <div class="campi">
                                <div class="parametri">
                                        <div class="group">
                                                <label for="password_1" class="label">Nuova Password</label>
                                                <input id="password_1" type="password" class="input" data-type="password"><span id="Controllo_Password"/>
                                        </div>
										<div class="group">
                                                <label for="password_2" class="label">Conferma Password</label>
                                                <input id="password_2" type="password" class="input" data-type="password">
                                        </div>
                                        <div class="group">
                                                <input type="submit" class="button" value="Conferma" id="submit" name="submit">
                                        </div>
                                        <div class="hr"></div>
                                </div>
                        </div>
                </div>
        </div>
</div>
</form>
<footer>
<small>--WEBSOCCER�-- Bernardini Yuri & Achilli Mattia 2017�</small>
</footer>
<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../Java/Reimposta_Password.js"></script>
</body>
</html>
<?php
include("Close_DB.php");
?>