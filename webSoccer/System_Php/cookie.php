<html>
<head>
</head>
<body>
<center>
<h1>COOKIE POLICY</h1>
<br><br><br>
<h2>Cookie</h2>
<br>
I cookie sono piccole porzioni di dati che vengono memorizzate e utilizzate per migliorare l�esperienza di utilizzo di un sito.
Ad esempio possono ricordare temporaneamente le tue preferenze di navigazione per evitarti di selezionare tutte le volte la lingua, rendendo quindi le visite successive pi� comode e intuitive.
Oppure possono servire per fare dei �sondaggi anonimi� su come gli utenti navigano attraverso il sito, in modo da poterlo poi migliorare partendo da dati reali.
<br>
I cookie non registrano alcuna informazione personale su un utente e gli eventuali dati identificabili non verranno memorizzati. Se si desidera disabilitare l�uso dei cookie � necessario personalizzare le impostazioni del proprio computer impostando la cancellazione di tutti i cookie o attivando un messaggio di avviso quando i cookie vengono memorizzati. Per procedere senza modificare l�applicazione dei cookie � sufficiente continuare con la navigazione.
<br><br> 
<h2>I cookie non sono virus o programmi</h2>
<br>
I cookie non sono virus o programmi. I cookie sono solamente dati salvati in forma testuale nella forma �variabile=valore� (esempio: �dataAccessoSito=2014-01-20,14:23:15?). Questi dati possono essere letti solamente dal sito che li ha generati, e in molti casi hanno una data di scadenza, oltre la quale il browser li canceller� automaticamente.
<br>
Non tutti i cookie vengono utilizzati per lo stesso scopo: di seguito verranno specificate le diverse tipologie di cookie utilizzati da questo sito web.
<br><br>
<h2>Cookie indispensabili</h2>
<br>
Questi cookie sono essenziali al fine di consentire di spostarsi in tutto il sito ed utilizzare a pieno le sue caratteristiche. Senza questi cookie alcuni servizi non potranno funzionare correttamente.
<br><br>
<h2>Performance cookie</h2>
<br>
Questi cookie raccolgono informazioni su come gli utenti utilizzano un sito web, ma non memorizzano informazioni che identificano un visitatore. Questi cookie vengono utilizzati solo per migliorare il funzionamento del sito web.
Utilizzando il sito l�utente accetta che tali cookie possono essere memorizzati sul proprio dispositivo.
<br><br>
<h2>Cookie di funzionalit�</h2>
<br>
I cookie consentono al sito di ricordare le scelte fatte dall�utente (come la lingua o altre impostazioni speciali eventualmente disponibili) e forniscono funzionalit� avanzate personalizzate, e possono anche essere utilizzati per fornire i servizi richiesti. Utilizzando il sito l�utente accetta che tali cookie possono essere memorizzati sul proprio dispositivo.
<br><br><br>
<h2>Come gestire i cookie sul tuo PC</h2>
<br>
Ogni browser consente di personalizzare il modo in cui i cookie devono essere trattati. Per ulteriori informazioni consultare la documentazione del browser utilizzato.
<br>
Alcuni browser consentono la �navigazione anonima� sui siti web, accettando i cookie per poi cancellarli automaticamente al termine della sessione di navigazione. Per ulteriori informazioni riguardo la �navigazione anonima� consultare la documentazione del browser utilizzato.
<br><br>
Se  hai bisogno di ulteriori informazioni o se hai domande sulla politica della privacy di questo sito ti preghiamo di contattarci via email
</center>
</body>
</html>
